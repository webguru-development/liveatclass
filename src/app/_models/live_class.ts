import { User } from './user';
import { Deserializable } from './deserializable';

export class LiveClass  implements Deserializable{
  id: number;
  teacher_id: number;

  title: string='';
  description: string='';
  start_time: Date;
  end_time: Date;
  timezone: string='+00:00';
  
  is_recurring: boolean=false;
  recurring_schedule: number=-1;
  recurring_class_end_type: boolean=false;
  recurring_class_limit: number=1;
  recurring_till: Date;
  on_monday: boolean=false;
  on_tuesday: boolean=false;
  on_wednesday: boolean=false;
  on_thursday: boolean=false;
  on_friday: boolean=false;
  on_saturday: boolean=false;
  on_sunday: boolean=false;

  record_on_entry: boolean=false;
  usermedia_on_entry: boolean=false;
  private_chat: boolean=false;
  is_live: boolean=false;
  is_closed: boolean=false;
  
  teacher: User;
  students=[];
  groups=[];
  created_at: Date;
  updated_at: Date;
  deserialize(input: any): this {
    Object.assign(this, input);
    this.is_recurring=(input.is_recurring==1);
    this.on_monday=(input.on_monday==1);
    this.on_tuesday=(input.on_tuesday==1);
    this.on_wednesday=(input.on_wednesday==1);
    this.on_thursday=(input.on_thursday==1);
    this.on_friday=(input.on_friday==1);
    this.on_saturday=(input.on_saturday==1);
    this.on_sunday=(input.on_sunday==1);
    this.record_on_entry=(input.record_on_entry==1);
    this.usermedia_on_entry=(input.usermedia_on_entry==1);
    this.private_chat=(input.private_chat==1);
    this.is_live=(input.is_live==1);
    this.is_closed=(input.is_closed==1);
    this.teacher = new User().deserialize(input.teacher);
    return this;
  }
}