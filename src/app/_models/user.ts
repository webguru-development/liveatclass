﻿import {Deserializable} from './deserializable';
export class User {
    id: number;
    email: string;
    password: string;
    confirmpassword: string;
    firstname: string;
    lastname: string;
    gender: number;	
    be_plan: number;
    user_type_id: number;
    skills:string;
    city:string;
    pin_code:string;
    oldpassword:string;	
    phone_number:string;	
    userType:string;	
    country:string;	
    timezone:string;	
    address:string;	
    captcha:string;
    activeLanguage:any;
    coursename:string;
    message:string;
    fb:string;
    tw:string;
    youtube:string;
    course_summary:string;
    content_article:string;
    lesson_content = "test content";
    logo:string="/assets/images/logo.png";
    profile_image:string="";
    category_name:string;
    name: string;
    description: string;
    date:string;
    duration:number;
    conversation:string;
    assignmentResult:string;
    group_name:string;
    
    /* start variables for login setting page */
    left_title:string;
    right_tilte:string;
    left_description:string;
    right_description:string;
    copyright_title:string;
    banner_image:string;
    /* end variable for loging setting page */
    
    /* start variable for loging setting page */
	course_title:string;
	sub_title:string;
	course_highlights:string;
	enroll_limit:number;
	course_detail:string;
	promotion_video:string;
	keywords:string;
	course_image:string;
	course_language:string;
	start_date:string;
	end_date:string;
	assigned_teacher:string;
	instructional_level:string;
	course_completetion_rule:string;
    courses:any;
    specialization:any;
	
    /* end variable for loging setting page */
    deserialize(input: any): this {
        return Object.assign(this, input);
    }
}
