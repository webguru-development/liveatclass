import { Component, OnInit, DoCheck, Inject, ViewChild, OnDestroy } from '@angular/core';
import {CmsPageService} from '../_services/cms-page.service';
import {Params, Router,ActivatedRoute} from '@angular/router';
import { ModalDirective } from 'angular-bootstrap-md';
import { DataTableDirective } from 'angular-datatables';
import { SortablejsOptions } from 'ngx-sortablejs';

@Component({
   //moduleId: module.id,
  templateUrl: './cms_page_list.component.html'
})

export class CmsPageListComponent implements OnInit,DoCheck {
	@ViewChild(DataTableDirective, {static: false})
	private datatableElement: DataTableDirective;
	dtOptions: any = {};
	operation={lifecycle: 'started',status: false,message: ''};
 	
 	sortOpts: SortablejsOptions={};
 	tmpPage={};
 	cmsPageList = [];
 	constructor(private router: Router, private CmsPageService: CmsPageService,private route: ActivatedRoute) {    		
  }

  ngOnInit() {
		const that = this;
		that.sortOpts.onUpdate=(e:any)=>{
 			that.CmsPageService.sortPages(e.oldIndex,e.newIndex).subscribe(resp=>{});
 		}
 		that.dtOptions={
			pagingType: 'simple_numbers',
			language: {
				search: "_INPUT_",
				searchPlaceholder: "Search by Page Name"
			},
			serverSide: true,
			processing: true,
			dom: 'tip',
				buttons: [
				{
					extend:    '',
					text:      '<i class="fa fa-share-square-o" aria-hidden="true"></i>'
				},
				{
					extend:    'csvHtml5',
					titleAttr: 'CSV'
				},
				{
					extend:    'excelHtml5',
					titleAttr: 'Excel'
				},
				{
					extend:    'pdfHtml5',
					titleAttr: 'PDF'
				}
			],
			columns: [{ data: 'title','orderable': false },{ data: 'user_id', 'searchable': false, 'orderable': false}, {data: 'created_at', 'searchable': false, 'orderable': false},{ data: 'id', 'searchable': false, 'orderable': false }],
			order: [[ 0, "asc" ]],
			ajax: (opts,callback)=>{
				that.CmsPageService.getAllPages(opts).subscribe(resp => {
					that.cmsPageList = resp.data;
					callback({
					  recordsTotal: resp.total,
					  recordsFiltered: resp.total,
					  data: []
					})
				});
			}
		};
	}
  
	ngDoCheck(){
		
	}
	changeStatus(page){
		this.CmsPageService.changeStatus(page.id,page.active)
			.subscribe(
				data => {
					 
				},
				error => {
					
				}
			);
	}  
    
	
	deleteCmsPage(page){
		this.CmsPageService.deleteCmsPage(page.id)
				.subscribe(
					data => { 
						this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
							dtInstance.ajax.reload();
						});
					},
					error => {

					}
				);   
	} 
	
 }