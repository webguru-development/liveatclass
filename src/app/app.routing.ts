﻿import { Routes, RouterModule } from '@angular/router';

import { LoginSettingComponent } from './accounts_settings/login-settings.component';
import { LogoSettingComponent } from './accounts_settings/logo-settings.component';
import { InterfaceComponent } from './accounts_settings/interface.component';
import { DomainComponent } from './accounts_settings/domain.component';
import { AddcourseComponent } from './course/add_course.component';
import { ManageCourselistComponent } from './course/manage_course_list.component';
import { MyCourselistComponent } from './course/my_course_list.component';
import { CoursePreviewComponent } from './course/course_preview.component';
import { StorageComponent } from './accounts_settings/storage.component';
import { UserListComponent } from './accounts_settings/user-lists.component';
import { EditPersonalDetailComponent } from './accounts_settings/edit-personal-detail.component';

import { MenuBarComponent } from './menu-bar.component';
import { ProfileComponent } from './profile/profile.component';
import { AppComponent } from './app.component';

import { NewpasswordComponent } from './login/new_password.component';
import { ActivateAccountComponent } from './login/activate_account.component';
import { LoginRouteGuard } from './login-route-guard';

import { CourseDetailComponent } from './course/course_detail.component';
import { CourseCatalogueComponent } from './course/course_catalogue.component';
import { CurriculumPlayComponent } from './course/curriculum_play.component';

import { DashboardComponent } from './institute/dashboard.component';
import { UserProfileCompnent } from './profile/user_profile.component';
import { FileLibraryComponent } from './library/file_library.component';
import { RecordingComponent } from './library/recording.component';
import { EmailTemplateComponent } from './email_templates/email_templates.component';
import { CmsPageListComponent } from './cms_pages/cms_page_list.component';
import { CmsPageFormComponent } from './cms_pages/cms_page_form.component';
import { PlanDetailComponent } from './plan/plan_detail.component';
import { PlanUpgradeComponent } from './plan/plan_upgrade.component';
import { LiveClassFormComponent } from './live_classes/live_class_form.component';
import { LiveClassListComponent } from './live_classes/live_class_list.component';
import { EnrolledLiveClassListComponent } from './live_classes/enrolled_live_class_list.component';
import { GroupComponent } from './groups/group.component';
import { GroupDetailComponent } from './groups/group_detail.component';
import { TeacherDashoardComponent } from './institute/teacher_dashboard.component';
import { StudentDashoardComponent } from './institute/student_dashboard.component';
import { LoginComponent } from './login/login.component';

import { PageComponent } from './page.component';
import { ComingSoonComponent } from './coming_soon.component';


const appRoutes: Routes = [
    
    { path: '', component: LoginComponent ,data: {theme: 'frontend'}},
    { path: 'login-settings', component: LoginSettingComponent,canActivate: [LoginRouteGuard] ,data: {theme: 'backend'}},
    { path: 'plan/detail', component: PlanDetailComponent,canActivate: [LoginRouteGuard] ,data: {theme: 'backend'}},
    { path: 'plan/upgrade', component: PlanUpgradeComponent,canActivate: [LoginRouteGuard] ,data: {theme: 'backend'}},
    { path: 'logo-settings', component: LogoSettingComponent,canActivate: [LoginRouteGuard],data: {theme: 'backend'}},
    { path: 'interface-language', component: InterfaceComponent,canActivate: [LoginRouteGuard],data: {theme: 'backend'}},
    { path: 'domain', component: DomainComponent,canActivate: [LoginRouteGuard],data: {theme: 'backend'}},
    { path: 'storage', component: StorageComponent,canActivate: [LoginRouteGuard],data: {theme: 'backend'}},
    
    
    //{ path: 'profile', component: ProfileComponent ,canActivate: [LoginRouteGuard]}, 
    { path: 'profile/:user_id', component: ProfileComponent ,data: {theme: 'backend'}}, 
    { path: 'new-password', component: NewpasswordComponent,canActivate: [NewpasswordComponent] ,data: {theme: 'frontend'} },
    { path: 'activate-account', component: ActivateAccountComponent,canActivate: [ActivateAccountComponent] ,data: {theme: 'blank'} },
    { path: 'course_preview/:id', component: CoursePreviewComponent ,data: {theme: 'course-preview'}},
    { path: 'course_catalogue', component: CourseCatalogueComponent ,data: {theme: 'frontend'}},
    
    { path: 'user-lists', component: UserListComponent,canActivate: [LoginRouteGuard]},
    { path: 'add-course', component: AddcourseComponent,canActivate: [LoginRouteGuard]},
    { path: 'add-course/:id', component: AddcourseComponent,canActivate: [LoginRouteGuard]},
    { path: 'courses_list', component: ManageCourselistComponent ,canActivate: [LoginRouteGuard]},
    { path: 'my_courses', component: MyCourselistComponent ,canActivate: [LoginRouteGuard]},
    { path: 'course_detail/:id', component: CourseDetailComponent ,canActivate: [LoginRouteGuard]},
    { path: 'play-mode/:id', component: CurriculumPlayComponent ,canActivate: [LoginRouteGuard] ,data: {theme: 'curriculum'}},
    
    { path: 'dashboard', component: DashboardComponent,canActivate: [LoginRouteGuard]},
    { path: 'edit_user_detail/:id', component: EditPersonalDetailComponent,canActivate: [LoginRouteGuard]},
    { path: 'user_profile', component: UserProfileCompnent,canActivate: [LoginRouteGuard]},
    { path: 'file_library', component: FileLibraryComponent,canActivate: [LoginRouteGuard]},
    { path: 'file_library/recordings', component: RecordingComponent,canActivate: [LoginRouteGuard]},
    { path: 'notifications', component: EmailTemplateComponent,canActivate: [LoginRouteGuard]},
    { path: 'cms_pages', component: CmsPageListComponent,canActivate: [LoginRouteGuard]},
    { path: 'cms_pages/add', component: CmsPageFormComponent,canActivate: [LoginRouteGuard]},
    { path: 'cms_pages/edit/:id', component: CmsPageFormComponent,canActivate: [LoginRouteGuard]},
    { path: 'live_classes', component: LiveClassListComponent,canActivate: [LoginRouteGuard]},
    { path: 'live_classes/enrolled', component: EnrolledLiveClassListComponent,canActivate: [LoginRouteGuard]},
    { path: 'live_classes/add', component: LiveClassFormComponent,canActivate: [LoginRouteGuard]},
    { path: 'live_classes/edit/:id', component: LiveClassFormComponent,canActivate: [LoginRouteGuard]},
    { path: 'groups', component: GroupComponent,canActivate: [LoginRouteGuard]},
    { path: 'group_detail/:id', component: GroupDetailComponent,canActivate: [LoginRouteGuard]},
    { path: 'teacher_dashboard', component: TeacherDashoardComponent,canActivate: [LoginRouteGuard]},
    { path: 'student_dashboard', component: StudentDashoardComponent,canActivate: [LoginRouteGuard]},
    { path: 'coming_soon', component: ComingSoonComponent,canActivate: [LoginRouteGuard]},

    // last Path to match
    { path: ':slug', component: PageComponent ,data: {theme: 'frontend-page'}},
    //{ path: '**', component: PageComponent ,data: {theme: 'frontend-page'}}
];


export const routing = RouterModule.forRoot(appRoutes);
