import { TestBed } from '@angular/core/testing';

import { FileLibraryService } from './file-library.service';

describe('FileLibraryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FileLibraryService = TestBed.get(FileLibraryService);
    expect(service).toBeTruthy();
  });
});
