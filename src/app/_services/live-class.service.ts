import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders,HttpParams} from '@angular/common/http';
import { RequestOptions, Response} from '@angular/http';
import { LiveClass } from '../_models/live_class';
import {environment} from '../../environments/environment';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class LiveClassService {

  constructor(private http: HttpClient) { }
  
  getClasses(dtParams:any){
    let opts=new HttpParams({
      fromObject: {
        limit: dtParams.length,
        offset: dtParams.start,
        page: ((dtParams.start/dtParams.length) + 1).toString(),
        search: dtParams.search.value,
        order: dtParams.order.length ? dtParams.columns[dtParams.order[0].column].name : '',
        dir: dtParams.order.length ? dtParams.order[0].dir : ''
      }
    });
    return this.http.get<any>(environment.apiUrl+'/live_classes',{params: opts})
          .map((res) => {
            res.data = res.data.map((liveClass: LiveClass) => new LiveClass().deserialize(liveClass)); 
            return res;
          });
  }
  getEnrolledClasses(dtParams:any){
    let opts=new HttpParams({
      fromObject: {
        limit: dtParams.length,
        offset: dtParams.start,
        page: ((dtParams.start/dtParams.length) + 1).toString(),
        search: dtParams.search.value,
        order: dtParams.order.length ? dtParams.columns[dtParams.order[0].column].name : '',
        dir: dtParams.order.length ? dtParams.order[0].dir : ''
      }
    });
    return this.http.get<any>(environment.apiUrl+'/live_classes/enrolled',{params: opts})
          .map((res) => {
            res.data = res.data.map((liveClass: LiveClass) => new LiveClass().deserialize(liveClass)); 
            return res;
          });
  }
  saveClass(classObj){
    if(classObj.id){
      return this.http.put<any>(environment.apiUrl+'/live_classes/'+classObj.id, classObj);
    }else{
      return this.http.post<any>(environment.apiUrl+'/live_classes', classObj);
    }
  }
  getClass(classId){
    return this.http.get<any>(environment.apiUrl+'/live_classes/'+classId).map((res) => {
            res.data = new LiveClass().deserialize(res.data); 
            return res;
          });
  }
  deleteClasses(ids){

    return this.http.post<any>(environment.apiUrl+'/live_classes/delete_classes',{id: ids});
  }
}
