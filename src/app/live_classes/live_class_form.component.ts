import { Component, OnInit, DoCheck, Inject, ViewChild, Input, ElementRef } from '@angular/core';
import {LiveClassService} from '../_services/live-class.service';
import { UserService } from '../_services/user.service';
import { GroupService } from '../_services/group.service';
import { LiveClass } from '../_models/live_class';
import {Params, Router,ActivatedRoute} from '@angular/router';
import { ModalDirective } from 'angular-bootstrap-md';
import { DataTableDirective } from 'angular-datatables';
declare var jQuery: any;
import * as types from 'gijgo';
import * as moment from 'moment';
@Component({
   //moduleId: module.id,
  templateUrl: './live_class_form.component.html'
})

export class LiveClassFormComponent implements OnInit,DoCheck {
  @ViewChild('assignParticipantsModal', { static: true }) assignParticipantsModal: ModalDirective;
  @ViewChild('conflictParticipantsModal', { static: true }) conflictParticipantsModal: ModalDirective;
  @ViewChild('assignGroupsModal', { static: true }) assignGroupsModal: ModalDirective;
  @ViewChild('start_time', { static: false }) start_time: ElementRef;
  @ViewChild('end_time', { static: false }) end_time: ElementRef;
  @ViewChild('recurring_till', { static: false }) recurring_till: ElementRef;
  @ViewChild('clipboard_data', { static: false }) clipboard_data: ElementRef;
  @Input() start_time_instance: types.DatePicker;
	@Input() end_time_instance: types.DatePicker;
  @Input() recurring_till_instance: types.DatePicker;
  startTimeConfiguration={};
  endTimeConfiguration={};
  recurringTillConfiguration={};
  operation={lifecycle: 'started',status: false,message: ''};
 	liveClass:LiveClass=new LiveClass();
  teachers=[];
  students=[];
  conflictStudents=[];
  groups:any;
 	selectedTeacher=0;
  selectedStudents={};
  selectedGroups={};
  plan={no_of_users_per_class: 20,max_class_duration: 120};
  currentUser:any;
  autoTimeZone=true;
  classDuration=30;
  isMinDurationSpecified=true;
  isMaxDurationSpecified=true;
  timezones=[
    { offset: '-11:00', label: '(GMT-11:00) Pago Pago', tzCode: 'Pacific/Pago_Pago' },
    { offset: '-10:00', label: '(GMT-10:00) Hawaii Time', tzCode: 'Pacific/Honolulu' },
    { offset: '-10:00', label: '(GMT-10:00) Tahiti', tzCode: 'Pacific/Tahiti' },
    { offset: '-09:00', label: '(GMT-09:00) Alaska Time', tzCode: 'America/Anchorage' },
    { offset: '-08:00', label: '(GMT-08:00) Pacific Time', tzCode: 'America/Los_Angeles' },
    { offset: '-07:00', label: '(GMT-07:00) Mountain Time', tzCode: 'America/Denver' },
    { offset: '-06:00', label: '(GMT-06:00) Central Time', tzCode: 'America/Chicago' },
    { offset: '-05:00', label: '(GMT-05:00) Eastern Time', tzCode: 'America/New_York' },
    { offset: '-04:00', label: '(GMT-04:00) Atlantic Time - Halifax', tzCode: 'America/Halifax' },
    { offset: '-03:00', label: '(GMT-03:00) Buenos Aires', tzCode: 'America/Argentina/Buenos_Aires' },
    { offset: '-02:00', label: '(GMT-02:00) Sao Paulo', tzCode: 'America/Sao_Paulo' },
    { offset: '-01:00', label: '(GMT-01:00) Azores', tzCode: 'Atlantic/Azores' },
    { offset: '+00:00', label: '(GMT+00:00) UTC', tzCode: 'Europe/London' },
    { offset: '+01:00', label: '(GMT+01:00) Berlin', tzCode: 'Europe/Berlin' },
    { offset: '+02:00', label: '(GMT+02:00) Helsinki', tzCode: 'Europe/Helsinki' },
    { offset: '+03:00', label: '(GMT+03:00) Istanbul', tzCode: 'Europe/Istanbul' },
    { offset: '+04:00', label: '(GMT+04:00) Dubai', tzCode: 'Asia/Dubai' },
    { offset: '+04:30', label: '(GMT+04:30) Kabul', tzCode: 'Asia/Kabul' },
    { offset: '+05:00', label: '(GMT+05:00) Maldives', tzCode: 'Indian/Maldives' },
    { offset: '+05:30', label: '(GMT+05:30) India Standard Time', tzCode: 'Asia/Calcutta' },
    { offset: '+05:45', label: '(GMT+05:45) Kathmandu', tzCode: 'Asia/Kathmandu' },
    { offset: '+06:00', label: '(GMT+06:00) Dhaka', tzCode: 'Asia/Dhaka' },
    { offset: '+06:30', label: '(GMT+06:30) Cocos', tzCode: 'Indian/Cocos' },
    { offset: '+07:00', label: '(GMT+07:00) Bangkok', tzCode: 'Asia/Bangkok' },
    { offset: '+08:00', label: '(GMT+08:00) Hong Kong', tzCode: 'Asia/Hong_Kong' },
    { offset: '+08:30', label: '(GMT+08:30) Pyongyang', tzCode: 'Asia/Pyongyang' },
    { offset: '+09:00', label: '(GMT+09:00) Tokyo', tzCode: 'Asia/Tokyo' },
    { offset: '+09:30', label: '(GMT+09:30) Central Time - Darwin', tzCode: 'Australia/Darwin' },
    { offset: '+10:00', label: '(GMT+10:00) Eastern Time - Brisbane', tzCode: 'Australia/Brisbane' },
    { offset: '+10:30', label: '(GMT+10:30) Central Time - Adelaide', tzCode: 'Australia/Adelaide' },
    { offset: '+11:00', label: '(GMT+11:00) Eastern Time - Melbourne, Sydney', tzCode: 'Australia/Sydney' },
    { offset: '+12:00', label: '(GMT+12:00) Nauru', tzCode: 'Pacific/Nauru' },
    { offset: '+13:00', label: '(GMT+13:00) Auckland', tzCode: 'Pacific/Auckland' },
    { offset: '+14:00', label: '(GMT+14:00) Kiritimati', tzCode: 'Pacific/Kiritimati' }
  ];
  constructor(private router: Router,private GroupService: GroupService, private UserService: UserService, private LiveClassService: LiveClassService,private route: ActivatedRoute) {    		
    let that=this;
    that.startTimeConfiguration = {
      footer: true,
      format: 'yyyy-mm-dd HH:MM TT',
      datepicker: {
        minDate: moment().subtract(1, 'days').toDate()
      },
      value: '',
      change: function(e){
        //@ts-ignore
        that.liveClass.start_time=moment(that.start_time_instance.value(),'YYYY-MM-DD HH:mm A').format('YYYY-MM-DD HH:mm:ss');
      }
    };
    that.endTimeConfiguration = {
      footer: true,
      format: 'yyyy-mm-dd HH:MM TT',
      datepicker: {
        minDate: moment().subtract(1, 'days').toDate()
      },
      value: '',
      change: function(e){
        let lastDate=that.end_time_instance.value();
        //@ts-ignore
        that.liveClass.end_time=moment(lastDate,'YYYY-MM-DD HH:mm A').format('YYYY-MM-DD HH:mm:ss');
      }
    };
    that.recurringTillConfiguration = {
      footer: true,
      format: 'yyyy-mm-dd',
      minDate: this.liveClass.start_time,
      value: '',
      change: function(e){
        //@ts-ignore
        that.liveClass.recurring_till=moment(that.recurring_till_instance.value()).format('YYYY-MM-DD');
      }
    };
  }
  isMinSatisfied(){
    if(!this.liveClass.end_time || !this.liveClass.start_time ) return true;
    this.isMinDurationSpecified=moment(this.liveClass.end_time).diff(moment(this.liveClass.start_time),'minutes') >= this.classDuration;
    return this.isMinDurationSpecified;
  }
  isMaxSatisfied(){
    if(!this.liveClass.end_time || !this.liveClass.start_time ) return true;
    this.isMaxDurationSpecified=moment(this.liveClass.end_time).diff(moment(this.liveClass.start_time),'minutes') <= this.plan.max_class_duration;
    return this.isMaxDurationSpecified;
  }
  isEndBeforeStart(){
    if(!this.end_time_instance || !this.liveClass.start_time) return false;

    let lastDate=this.end_time_instance.value();
    //@ts-ignore
    return (moment(this.liveClass.start_time) > moment(lastDate,'YYYY-MM-DD HH:mm A'));
  }
  ngOnInit() {
  	let that=this;
    that.currentUser=JSON.parse(localStorage.getItem('user_data'));
    if(that.currentUser['user_type']=='teacher') that.liveClass.teacher_id=that.currentUser['id'];

    this.UserService.getPlanInfo().subscribe(resp=>{
      this.plan=resp;
    },err=>{
      //ToDo: do something with error      
    });

    this.GroupService.getGroups({length: -1,start: 0,search: {value: ''},order: []})
    .subscribe(
      resp => {
        this.groups = resp.data; 
      },
      error => {
        //ToDo: Do something with errors
      }
    );

    this.UserService.getTeacherList()
    .subscribe(
      data => {
        this.teachers = data['getTeacherRecord']; 
      },
      error => {
        //ToDo: Do something with errors
      }
    );
    this.UserService.getStudentsList(0,true)
    .subscribe(
      data => {
        this.students = data; 
      },
      error => {
        //ToDo: Do something with errors
      }
    );   
    that.liveClass.timezone=moment().format("Z");
    that.route.params.subscribe(params => {
  		if(params['id']){
  			this.LiveClassService.getClass(params['id']).subscribe(resp=>{
	  			this.liveClass=resp.data;
          this.selectedStudents={};
          this.liveClass.students.forEach(student=>{
            this.selectedStudents[student.student_id]=true;
          });
          this.liveClass.start_time=moment.utc(this.liveClass.start_time).utcOffset(this.liveClass.timezone).toDate();
          this.liveClass.end_time=moment.utc(this.liveClass.end_time).utcOffset(this.liveClass.timezone).toDate();
          this.startTimeConfiguration['value']=moment(that.liveClass.start_time).format('YYYY-MM-DD HH:mm A');
          this.endTimeConfiguration['value']=moment(that.liveClass.end_time).format('YYYY-MM-DD HH:mm A');
          this.recurringTillConfiguration['value']=moment(that.liveClass.recurring_till).format('YYYY-MM-DD');
          this.autoTimeZone=false;
          this.initDatepickers();
	  		},error=>{
	  			//Todo: do something with error
	  		})	
  		}else{
        this.initDatepickers();
      }
  	});
	}
  ngDoCheck(){
		
	}
  resetToLocal(){
    this.autoTimeZone=!this.autoTimeZone;
    if(this.autoTimeZone) this.liveClass.timezone=moment().format("Z");
  }
  initDatepickers(){
    setTimeout(_=>{
      // giving datepicker not a function error, so delaying it a bit
      if(this.start_time) this.start_time_instance = jQuery(this.start_time.nativeElement).datetimepicker(this.startTimeConfiguration); 
      if(this.end_time) this.end_time_instance = jQuery(this.end_time.nativeElement).datetimepicker(this.endTimeConfiguration); 
      if(this.recurring_till) this.recurring_till_instance = jQuery(this.recurring_till.nativeElement).datepicker(this.recurringTillConfiguration); 
    },300);
  }
  ngAfterViewInit() {
    
  }
  
  ngOnDestroy() {
    if(this.start_time_instance) this.start_time_instance.destroy();
    if(this.end_time_instance) this.end_time_instance.destroy();
    if(this.recurring_till_instance) this.recurring_till_instance.destroy();
  }
  getClassUrl(classId){
    return window.location.origin+'/class/?discussion_id='+classId;
  }
  copy2Clip(classId){
    this.clipboard_data.nativeElement.select();
    this.clipboard_data.nativeElement.setSelectionRange(0, 99999);
    document.execCommand("copy");
  }
  assignTeacher(){
    this.teachers.forEach((teacher)=>{
      if(this.selectedTeacher==teacher.user_id){
        this.liveClass.teacher=teacher;
        this.liveClass.teacher_id=teacher.user_id;
      }
    });
  }
  assignGroups(){
    this.operation={lifecycle: 'running',status: false,message: ''};
    
    let selectedGroups=[];
    let that=this;
    that.liveClass.groups=[];
    Object.keys(that.selectedGroups).forEach(function(key){
      if(that.selectedGroups[key]){
        that.liveClass.groups.push({group_id: key});
      }
    });
    that.resetOperation();
    that.assignGroupsModal.hide();  
  }
  assignParticipants(){
    this.operation={lifecycle: 'running',status: false,message: ''};
    
    let selectedStudents=[];
    let that=this;
    that.liveClass.students=[];
    Object.keys(that.selectedStudents).forEach(function(key){
      if(that.selectedStudents[key]){
        that.liveClass.students.push({student_id: key});
      }
    });
    if( that.plan.no_of_users_per_class < that.liveClass.students.length ){
      that.liveClass.students=[];
      this.operation={lifecycle: 'stopped',status: false,message: 'Your current plan supports maximum of <b class="grn-txt-clr">'+that.plan.no_of_users_per_class+' participants</b> only.'};
    }else{
      that.resetOperation();
      that.assignParticipantsModal.hide();  
    }
  }
  resetOperation(){
    this.operation={lifecycle: 'started',status: true,message: ''};
  }
	saveClass(){
		this.operation={lifecycle: 'running',status: false,message: ''};
 		this.conflictStudents=[];
    if(this.currentUser['user_type']=='teacher'){
      this.liveClass.teacher_id=this.currentUser['id'];
    }

    this.LiveClassService.saveClass(this.liveClass)
			.subscribe(
				data => {
					this.operation={lifecycle: 'stopped',status: true,message: 'Class saved successfully'};
 					this.router.navigate(['/live_classes']); 
				},
				error => {
          if(error.data && error.data.data.students){
            this.conflictStudents=error.data.data.students;
          }
          this.operation={lifecycle: 'stopped',status: false,message: error.message};
        }
			);
	}
	
 }