import {Component,Input,DoCheck,OnInit} from '@angular/core';
import {UserService} from './_services/user.service';
import {User} from './_models/user';
import { Router } from '@angular/router';
import * as $ from "jquery";


@Component({

  selector: 'menu-page',
  templateUrl: './menu-bar.component.html'

})

export class MenuBarComponent implements OnInit {
   	model: any = {};
   	lang:any;
   	getLang:any;
   	public file_srcs: string='';
   	category:any;
   	logo:any;
   	showMenu= false;
   	@Input() theme: string;
   	@Input() site_settings;
   	@Input() course;
   	@Input() currentUser;
   	courseTitle='';
	constructor(private UserService: UserService, private router:Router,private user:User) {
	}

	ngOnInit(){
		this.getLangList();
		if(JSON.parse(localStorage.getItem("language"))==null){
			this.language("English");
		}else{
			this.lang = JSON.parse(localStorage.getItem("language"));
		}
		
		this.file_srcs=("/assets/images/dummy.png");
		
		this.UserService.currentCourse.subscribe(title => this.courseTitle = title)
			//~ if(this.currentUser['user_data']['profile_image'] ==  null){
				//~ this.file_srcs=("/assets/images/dummy.png");
			//~ }else{
				//~ this.file_srcs=(this.currentUser['user_data']['profile_image']);
			//~ }
			
		
	}

	ngDoCheck(){
		if(this.lang!=JSON.parse(localStorage.getItem("language")))
		{
			this.lang=JSON.parse(localStorage.getItem("language")); 
		}
	}
	language(language:string){

		this.UserService.languageRequest(language)
			.subscribe(
				data => {  
					
					localStorage.setItem("language", JSON.stringify(data['json']));
					localStorage.setItem("plan", JSON.stringify(data['plan']));
					localStorage.setItem("activeLanguage",JSON.stringify(language));
				},
				error => {

				}
			);                        
	}
	
	
	getLangList(){
	this.UserService.getLangList()
			.subscribe(
				data => {  
				
					this.getLang = data;
				},
				error => {

				}
			);   

	}

	logout(){
	  localStorage.removeItem('permissions');
	  localStorage.removeItem('user_data');
	  localStorage.removeItem('token');
	  this.router.navigate(['/']);
	} 

	signup(id:string)
	{
		if(id=='1'){
			this.router.navigate(['signup-student']);
		}else if(id=='2'){
			this.router.navigate(['signup-teacher']);
		}else{
			this.router.navigate(['signup-institute']);
		}
				 
	}

	getcategories(){
		this.UserService.getcategories()
			.subscribe(
				data => { 
					this.category=data; 
				},
				error => {

				}
			);   
	}

	toggleMenu(){
		this.showMenu=!this.showMenu;
	}
}
