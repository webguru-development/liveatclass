import { Directive,  Output, EventEmitter, HostBinding, HostListener, ElementRef } from '@angular/core';

@Directive({
  selector: '[appDragdrop]'
})
export class DragdropDirective {
  @Output() onFileDropped = new EventEmitter<any>();
  
  @HostBinding('class.dragging') private isDragging:boolean = false;
  //Dragover listener
  @HostListener('dragover', ['$event']) onDragOver(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.isDragging = true;
  }
  //Dragleave listener
  @HostListener('dragleave', ['$event']) public onDragLeave(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.isDragging = false;
  }
  //Drop listener
  @HostListener('drop', ['$event']) public ondrop(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.isDragging = false;
    let files = evt.dataTransfer.files;
    if (files.length > 0) {
      this.onFileDropped.emit(files)
    }
  }
}
