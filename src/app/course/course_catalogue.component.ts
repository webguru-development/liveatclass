import { Component, OnInit, DoCheck ,ViewChild} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser'
import {UserService} from '../_services/user.service';
import {User} from '../_models/user';
import {Params, Router, ActivatedRoute} from '@angular/router';
import * as $ from "jquery";
import {NgbRatingConfig} from '@ng-bootstrap/ng-bootstrap';
import { ModalDirective } from 'angular-bootstrap-md';

@Component({
    templateUrl: 'course_catalogue.component.html',
    providers: [NgbRatingConfig]
})

export class CourseCatalogueComponent implements OnInit, DoCheck {
	@ViewChild('popEnquiryMsgModal', { static: true }) popEnquiryMsgModal: ModalDirective;
	@ViewChild('enrollToCourseModal', { static: true }) enrollToCourseModal: ModalDirective;
	limit = 8;
    page:number = 1;
    lastpage = false;
    firstpage = false;
	searchText: string = '';
	coursesList : [];
	enrollCourse:any = {};
	courseId:any='';
	getCourseYear:any = [];
	
    constructor(config: NgbRatingConfig,private UserService: UserService, private router: Router, private route: ActivatedRoute , public sanitizer: DomSanitizer) {
		config.max = 5;
		config.readonly = true;
   
    }

   
	ngOnInit() {
			this.getCourseYears();
			this.getCourseList();
    }
  
    ngDoCheck(){
    
		
	}
	
	enrollToCourse(courseId){
		this.courseId=courseId;
		this.enrollToCourseModal.show();
	}
	
	courseEnrolledRequest(){
		this.UserService.courseEnrolledRequest(this.enrollCourse,this.courseId).subscribe(resp=>{
			this.enrollToCourseModal.hide();
			this.popEnquiryMsgModal.show()
		});
		
		//~ this.enrollToCourseModal.hide();
		//~ this.popEnquiryMsgModal.show()
	}
	
	onScroll(event)
	{	
		console.log(this.lastpage == true);
		if(this.lastpage == false){
			if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
				this.page = this.page+1;
				this.getCourseList();
			}
		}
		
	}
	
	onSearch(e: any) {
		if(e.which==13){
			this.getCourseList();
		}
	  }
	  
	clearSearch() {
		this.getCourseYear = [];
		this.getCourseYears();
		this.searchText = '';
		this.getCourseList();
	  }

	getCourseYears(){
		this.UserService.getCourseYears()
			.subscribe(
				resp => {  
					this.getCourseYear = resp.getCourseYear;
				},
				error => {

				}
			);
	}
	
	getCourseList(e: any = 'all'){
		this.UserService.getAllCourses(this.page, this.limit,this.searchText,e)
			.subscribe(
				resp => {  
					this.coursesList = resp.data;
				},
				error => {

				}
			);
	
	}
	 
 
}
