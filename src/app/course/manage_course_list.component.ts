import { Component, OnInit, DoCheck,ViewChild } from '@angular/core';
import {UserService} from '../_services/user.service';
import {User} from '../_models/user';
import {Params, Router} from '@angular/router';
import { AppComponent } from '../app.component';
import { ModalDirective } from 'angular-bootstrap-md';
import { DataTableDirective } from 'angular-datatables';

@Component({
    templateUrl: 'manage_course_list.component.html'
})

export class ManageCourselistComponent implements OnInit, DoCheck {
	@ViewChild('editCourseModal', { static: true }) editCourseModal: ModalDirective;
	@ViewChild('deleteCourseModal', { static: true }) deleteCourseModal: ModalDirective;
	@ViewChild('courseCloneModal', { static: true }) courseCloneModal: ModalDirective;
	@ViewChild('publishCourseModal', { static: true }) publishCourseModal: ModalDirective;
	@ViewChild('endCourseModal', { static: true }) endCourseModal: ModalDirective;
	@ViewChild(DataTableDirective, {static: false})
	private datatableElement: DataTableDirective;
	dtOptions: any = {};
    message = '';
    newmessage = '';
	data:any;
	model: any = {};
	selectedCourse: any = {};
    MyCourseList = [];
    currentUser:any;
    permissions:any;
    markcompleted= [];
    studentList = [];
    courseId:number;
    language:number;
    groupList = [];
    loadingComplete = false;
    constructor(private UserService: UserService, private router: Router,private appComponent: AppComponent) {}

	ngOnInit() {
		const that = this;
		that.data=JSON.parse(localStorage.getItem("language"));
		that.currentUser= JSON.parse(localStorage.getItem("user_data"));
		that.permissions= JSON.parse(localStorage.getItem("permissions"));
		that.language = JSON.parse(localStorage.getItem("activeLanguage"));
		let tableCols=[];
		let tableOrder=[ 0, "asc" ];
		if(that.currentUser.user_type=='superadmin' || that.currentUser.user_type=='admin'){
			tableCols.push({ 'searchable': false,'orderable': false });
			tableOrder=[ 1, "asc" ];
		}
		tableCols.push({ data: 'course_uid' });
		tableCols.push({ data: 'course_title' });
		tableCols.push({ 'searchable': false,'orderable': false,data:'assigned_teacher' });
		tableCols.push({ 'searchable': false,'orderable': false });
		if(that.currentUser.user_type=='superadmin' || that.currentUser.user_type=='admin'){
			tableCols.push({ data: 'is_active' });
		}
		that.dtOptions = {
						pagingType: 'simple_numbers',
						pageLength: 10,
						language: {
							search: "_INPUT_",
							searchPlaceholder: "Search by Course ID/Name"
						},
						bPaginate: true,
						bLengthChange: true,
						bInfo: true,
						bAutoWidth: false,
						serverSide: true,
						processing: true,
						ajax: (dataTablesParameters: any, callback) => {
							that.UserService.getMyCourseList(dataTablesParameters).subscribe(resp => {
								that.MyCourseList = resp.data;
								callback({
								  recordsTotal: resp.total,
								  recordsFiltered: resp.total,
								  data: []
								});
							});
						},
						columns: tableCols,
						order: [tableOrder]
					};
    }
  
    ngDoCheck(){
    if(this.data!=JSON.parse(localStorage.getItem("language"))){
		this.data=JSON.parse(localStorage.getItem("language")); 
		}
		if(this.newmessage!=""){
			this.message=this.newmessage;
		}
	}
	selectAllCourse(e){
		this.MyCourseList.forEach(course=>{
				this.selectedCourse[course.id]=false;
			});
		if(e.target.checked == true){
			this.MyCourseList.forEach(course=>{
				this.selectedCourse[course.id]=e.target.checked;
			});
		}
	}
	anySelected(){
		return Object.values(this.selectedCourse).some(val=>{ return val===true});
		
	}
	
	removeSelectedCourses(){
		let courseSelected=[];
		let that=this;
		Object.keys(that.selectedCourse).forEach(function(key){
			if(that.selectedCourse[key]) courseSelected.push(key);
		})
		if(courseSelected.length <=0) return;
		this.UserService.removeSelectedCourses(courseSelected.join(','))
			.subscribe(
					data => {
						this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
							dtInstance.ajax.reload();
							this.deleteCourseModal.hide();
							this.selectedCourse={};
						});
						 
					},
					error => {

					}
				);
	}
	//clone course start
	showCloneCoursePopup(courseId:number){
		this.courseId = courseId;
		this.courseCloneModal.show();
	}
	
	courseClone(){
		this.UserService.courseClone(this.courseId).subscribe(resp=>{
			this.courseCloneModal.hide();
			this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
				dtInstance.ajax.reload();
			});
		});
	}
	
	//clone course end
	
	showEditCoursePopup(courseId:number){
		this.courseId = courseId;
		this.editCourseModal.show();
	}
	
	editCourse(value:any){
		console.log(value);
		if(value.edit_course == 'learner_preview'){
			 this.router.navigate(['/course_preview',this.courseId], { queryParams: { userType: 'student' } });
		}
		if(value.edit_course == 'teacher_preview'){
			 this.router.navigate(['/course_preview',this.courseId], { queryParams: { userType: 'teacher' } });
		}
		if(value.edit_course == 'course_edit'){
			 this.router.navigate(['/add-course',this.courseId], { queryParams: { courseId: this.courseId } });
		}
		if(value.edit_course == 'course_material'){
			 this.router.navigate(['/course_detail',this.courseId]);
		}
		if(value.edit_course == 'course_publish'){
			//~ value.edit_course = '';
			this.editCourseModal.hide();
			this.publishCourseModal.show();
			 //~ this.router.navigate(['/course_preview',this.courseId], { queryParams: { userType: 'teacher' } });
		}
		
		if(value.edit_course == 'course_end'){
			//~ value.edit_course = '';
			this.editCourseModal.hide();
			this.endCourseModal.show();
			 //~ this.router.navigate(['/course_preview',this.courseId], { queryParams: { userType: 'teacher' } });
		}
			//~ console.log(value.edit_course);
	}
	
	
	coursePublish(courseId:number){
		
		this.UserService.coursePublish(courseId).subscribe(resp=>{
			this.publishCourseModal.hide();
			this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
				dtInstance.ajax.reload();
			});
		});
	}
	
	endCourse(courseId:number){
		
		this.UserService.endCourse(courseId).subscribe(resp=>{
			this.endCourseModal.hide();
			this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
				dtInstance.ajax.reload();
			});
		});
	}
	
	//~ getStudentsList(courseId:number){
		//~ this.courseId = courseId;
		//~ this.UserService.getStudentsList(courseId)
				//~ .subscribe(
					//~ data => { 
						//~ this.studentList = data;
	//~ 
					//~ },
					//~ error => {
//~ 
					//~ }
				//~ ); 
		//~ }
		//~ 
		//~ StudentEnrolltoCourse(){
		//~ this.appComponent.isLoading = true;
		//~ this.UserService.StudentEnrolltoCourse(this.courseId,this.language,$('[name^="studentList"]').serialize())
		//~ .subscribe(
				//~ data => {
					//~ this.appComponent.isLoading = false;
					//~ this.loadingComplete = true;
					 //~ 
				//~ },
				//~ error => {
					//~ this.appComponent.isLoading = false;
					//~ this.loadingComplete = true;
				//~ }
			//~ ); 
		//~ }
		//~ 
		//~ getGroupList(courseId:number){
			//~ this.courseId = courseId;
			//~ this.UserService.getGroupList(this.courseId)
					//~ .subscribe(
						//~ data => { 
						 //~ this.groupList = data['Groups'];
		//~ 
						//~ },
						//~ error => {
//~ 
						//~ }
					//~ ); 
		//~ }
		//~ 
		//~ assignCourseToGroup(){
			//~ this.appComponent.isLoading = true;
			//~ this.UserService.assignCourseToGroup(this.courseId,this.language,$('[name^="groupLists"]').serialize())
			//~ .subscribe(
					//~ data => {
						//~ this.appComponent.isLoading = false;
						//~ this.loadingComplete = true;
				//~ },
					//~ error => {
						//~ this.appComponent.isLoading = false;
						//~ this.loadingComplete = true;
					//~ }
				//~ ); 
		//~ }
}
