import { Component, OnInit, DoCheck, Inject, ViewChild } from '@angular/core';
import {UserService} from '../_services/user.service';
import {User} from '../_models/user';
import {Params, Router,ActivatedRoute} from '@angular/router';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { AppComponent } from '../app.component';
import { MomentModule } from 'angular2-moment';
import {NgbRatingConfig} from '@ng-bootstrap/ng-bootstrap';
import { ModalDirective } from 'angular-bootstrap-md';


@Component({
   //moduleId: module.id,
  templateUrl: './course_preview.component.html',
  providers: [NgbRatingConfig]
})

export class CoursePreviewComponent implements OnInit,DoCheck {
	public isCollapsed :any ={};
	@ViewChild('deleteReviewModal', { static: true }) deleteReviewModal: ModalDirective;
	model: any = {};
	reviewModel: any = {id:'',comment:'',rating:0  }
   	CourseID:number;
   	currentUser:any;
   	currentReview:any=null;
   	userType:string='teacher';
   	averageRating:string = '';
   	rating = 0;
   	assignedTeachers: [];
   	currentTab:string;
   	showMore:any={};
   	editReview:any={};
   	classes=0;
   	videos=0;
	public Editor = ClassicEditor;
	
  constructor(config: NgbRatingConfig,private router: Router, private UserService: UserService,private route: ActivatedRoute, private appComponent: AppComponent) {    		
		config.max = 5;
		config.readonly = true;
		this.currentTab='detail';
		
		this.route.queryParams.subscribe(params => {
			if(typeof params.userType != 'undefined'){
				this.userType=params.userType;
				//console.log(this.userType);
			}
		});
    }
     
	ngOnInit() {
		this.currentUser= JSON.parse(localStorage.getItem("user_data"));
		this.route.params.subscribe(params => {this.CourseID=params['id']});
		this.getCourseDetails();
	
	}
	ngDoCheck(){
		
	}
	
	
	getCourseDetails(){
		 this.UserService.getCourseDetail(this.CourseID)
			.subscribe(
				data => { 
					this.model = data['getCourseDetail'];
					this.assignedTeachers = data['teachers'];
					this.currentReview = data['getCurrentUserReview'];
					this.classes=data['classes'];
					this.videos=data['videos'];
					
					let avg=0;
					this.model.reviews.forEach(review=>{
						avg+=review.rating;
					});
					let avgRating=avg/this.model.reviews.length;
					this.averageRating=(isNaN(avgRating) ? 0 : avgRating).toFixed(2);
				},
				error => {

				}
			); 
		
	}
	
	addReview(){
		this.UserService.addReview(this.reviewModel,this.CourseID)
			.subscribe(
				data => {
					this.getCourseDetails();
					this.reviewModel.comment = '';
					this.reviewModel.id = '';
					this.reviewModel.rating = 0;
				},
				error => {

				}
			);
	}
	
	edit_review(review){
		this.UserService.addReview(review,this.CourseID)
			.subscribe(
				data => {
					this.getCourseDetails();
					this.editReview = {};
				},
				error => {

				}
			);
	}
	
	showDeleteReviewPopup(review){
		this.reviewModel = review;
		this.deleteReviewModal.show();
	}
	
	deleteReview(){
		this.UserService.deleteReview(this.reviewModel.id)
			.subscribe(
				data => {
					this.deleteReviewModal.hide();
					this.getCourseDetails();
					this.reviewModel.id = '';
					this.reviewModel.comment = '';
					this.reviewModel.rating = 0;
					this.currentReview={};
					
				},
				error => {

				}
			);
	}
	
	currentUserReviews(){
		
	}
	
	
}

			

																																																																																																																								

