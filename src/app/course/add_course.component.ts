import { Component, ElementRef, OnInit, DoCheck,ViewChild ,ViewChildren, Input ,ChangeDetectorRef,AfterViewInit,OnDestroy,QueryList} from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import {UserService} from '../_services/user.service';
import {FileLibraryService} from '../_services/file-library.service'; 
import {User} from '../_models/user';
import {Params, Router,ActivatedRoute} from '@angular/router';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
declare var jQuery: any;
import * as types from 'gijgo';
import {IMyDpOptions, IMyDateModel} from 'mydatepicker';
import { DataTableDirective } from 'angular-datatables';
import { ModalDirective } from 'angular-bootstrap-md';

@Component({
    templateUrl: 'add_course.component.html'
})

export class AddcourseComponent implements OnInit, DoCheck ,AfterViewInit, OnDestroy{
@ViewChild('course_image_select', { static: false }) course_image_select: ElementRef;
@ViewChild('course_start_date', { static: false }) course_start_date: ElementRef;
@ViewChild('course_end_date', { static: false }) course_end_date: ElementRef;
@ViewChild('selectTeacher', { static: true }) selectTeacher: ModalDirective;
@ViewChild('deleteTeacher', { static: true }) deleteTeacher: ModalDirective;
@ViewChild('embeddedContentModal', { static: true }) embeddedContentModal: ModalDirective;
@ViewChild('contentModal', { static: true }) contentModal: ModalDirective;
@ViewChild('videoContentModal', { static: true }) videoContentModal: ModalDirective;
//~ @ViewChild("datepicker" , { static: true }) datepicker: DatePickerComponent;
@ViewChildren(DataTableDirective)
dtElements: QueryList<DataTableDirective>;
@Input() course_start_date_instance: types.DatePicker;
@Input() course_end_date_instance: types.DatePicker;
@Input() startDateConfiguration: types.DatePickerSettings;
@Input() endDateConfiguration: types.DatePickerSettings;

 //~ configuration: types.DatePickerSettings;
 //~ date = '03/08/2019';
 //~ eventLog: string = '';
 public course_image_srcs: any= '';
 dtOptions: any = {};
 currentUser:any;
 teacherList:any;
 selectedCourseImage;
 selectedPromoVideo;
 courseImageFile;
 model:any={instructional_level:'1',course_language:'3' , courseImage:'',course_highlights:'',course_completion_rule:'1',course_detail:'',assignedTeachers: []};
 public file_srcs: any= '';
 courseImage:any= '';
 promoVideo:any= '';
 promotion_video:any= '';
 courseId:any = '';
 
 selectedTeachers= {};
 embedList=[];
 fileList=[];
 videoFileList=[];
 folderList=[];
 videoFolderList=[];
 showFileType:number=-1;
 imageFileType=['jpg','png'];
 videoFileType=['mp4','recordings','webm'];
 currentFolderId=0;
 currentVideoFolderId=0;
 isFile:boolean=true;
 isFolder:boolean=false;
 isEmbeded:boolean=false;
 unsanitizedVideoUrl='';
 languages=[{id:'3',name:'English'}];
 start_date:any = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
 end_date:any;
 currentTab: string = 'basic_settings';
 selectedTeacher:any;
 public Editor = ClassicEditor;
 
 myDatePickerOptions: IMyDpOptions = {
        // other options...
        dateFormat: 'yyyy-mm-dd',
    };
     
    constructor(private UserService: UserService,private FileLibraryService: FileLibraryService, private router: Router ,private route: ActivatedRoute,private changeDetectorRef:ChangeDetectorRef, public sanitizer: DomSanitizer) {
		let that=this;
		that.route.queryParams.subscribe(params => {
			if(typeof params.courseId != 'undefined'){
				that.courseId=params.courseId;
				that.getCourseDetails();
				
			}
		});
		that.startDateConfiguration = {
			format: 'yyyy-mm-dd',
			minDate: this.start_date,
			change: function(e){
					that.model.start_date=that.course_start_date_instance.value();
			}
		};
		that.endDateConfiguration = {
			format: 'yyyy-mm-dd',
			minDate: this.start_date,
			change: function(e){
					that.model.end_date=that.course_end_date_instance.value();
			}
		};
		
    }

	ngOnInit() {
		let that=this;
      	that.currentUser= JSON.parse(localStorage.getItem("user_data"));	
      	that.getTeacherList();
		let options={
			pagingType: 'simple_numbers',
			pageLength: 10,
			dom: 'fltipr',
			processing:true,
			serverSide: true,
			language: {
			  search: "_INPUT_",
			  searchPlaceholder: "Search by Name"
			},
			order: [[0, 'asc']],
		  };
		let detailTypes={
			imageFiles: {
				placeholder: 'Search by File/Folder',
				func: 'getFiles',
				classParam: 'fileList',
				service: 'FileLibraryService',
				params: ['currentFolderId','showFileType','imageFileType'], 
				columns: [
					{"name": 'name',"orderable": true},
					{"name": 'type',"orderable": true},
					{"name": 'id',"orderable": false},
				]
			},
			videoFiles: {
				placeholder: 'Search by File/Folder',
				func: 'getFiles',
				classParam: 'videoFileList',
				service: 'FileLibraryService',
				params: ['currentVideoFolderId','showFileType','videoFileType'], 
				columns: [
					{"name": 'name',"orderable": true},
					{"name": 'type',"orderable": true},
					{"name": 'id',"orderable": false},
				]
			},
			embedded_content: {
				placeholder: 'Search by File Name',
				func: 'getEmbeds',
				classParam: 'embedList',
				params: [],
				service: 'FileLibraryService',
				columns: [
					{"name": 'name',"orderable": true},
					{"name": 'contentType',"orderable": true},
					{"name": 'id',"orderable": false}
				]
			},
		};
		Object.keys(detailTypes).forEach(name=>{
			this.dtOptions[name] = JSON.parse(JSON.stringify(options));
			this.dtOptions[name]['language']['searchPlaceholder']=detailTypes[name]['placeholder'];
			this.dtOptions[name]['columns']=detailTypes[name]['columns'];
			this.dtOptions[name]['ajax'] =  (dataTablesParameters: any, callback) => {
			  let params=[dataTablesParameters];
			  detailTypes[name]['params'].forEach(param=>{ params.push(this[param])});
			  this[detailTypes[name]['service']][ detailTypes[name]['func'] ](...params).subscribe(resp => {
				this[detailTypes[name]['classParam']] = resp.data;
				callback({
				  recordsTotal: resp.total,
				  recordsFiltered: resp.total,
				  data: []
				});
			  });
			}
		});
    }
  
    ngDoCheck(){
   
	}
	
	ngAfterViewInit() {
		
		setTimeout(_=>{
				this.initPickers();
		},300)
	}  
  initPickers(){
  	if(!this.course_start_date_instance){
  		// giving datepicker not a function error, so delaying it a bit
			this.course_start_date_instance = jQuery(this.course_start_date.nativeElement).datepicker(this.startDateConfiguration); 
			this.course_end_date_instance = jQuery(this.course_end_date.nativeElement).datepicker(this.endDateConfiguration); 	
  	}
  }
  ngOnDestroy() {
    if(this.course_start_date_instance) this.course_start_date_instance.destroy();
    if(this.course_end_date_instance) this.course_end_date_instance.destroy();
  }
	getCourseDetails(){
		 this.UserService.getCourseDetail(this.courseId)
			.subscribe(
				data => { 
					this.model = data['getCourseDetail'];
					if(data['getCourseDetail'] && !!data['getCourseDetail']['course_image']){
						this.course_image_srcs=data['getCourseDetail']['course_image'];
				    }
					this.checkVideoFormat(this.model.promotion_video);
					if(this.isEmbeded==true){
						this.promoVideo=this.getFileSrc(this.model.promotion_video); 
					}
					this.courseAssignTeachers(data['teachers']);
					
				},
				error => {

				}
			); 
	}
	
	getYoutubeId(url) {
		const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
		const match = url.match(regExp);
		return (match && match[2].length === 11)
		  ? match[2]
		  : null;
	}
	
	getFileSrc(file){
		let filename=file;
		let youtubeId=this.getYoutubeId(filename);
		filename='//www.youtube.com/embed/'+ youtubeId;
		return this.sanitizer.bypassSecurityTrustResourceUrl(filename);
	}
	videoUrl(videoUrl){
		if(this.videoType(videoUrl)=='youtube'){
			return this.getFileSrc(videoUrl);
		}else{
			return this.sanitizer.bypassSecurityTrustResourceUrl(videoUrl);
		}
	}
	videoType(videoUrl){
		if(videoUrl){
			const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
			if(videoUrl.match(regExp)){
				return 'youtube';
			}else{
				return 'normal';
			}	
		}
		return 'none';
	}
	checkVideoFormat(videoUrl){
		if(videoUrl){
			const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
			if(videoUrl.match(regExp)){
				this.isEmbeded = true;
			}	
		}
		return this.isEmbeded;
	}
	
	getTeacherList(){
		let courseID = (this.courseId)? this.courseId : 0;
		//if(this.courseId ! = )
	this.UserService.getTeacherListByCourse(courseID)
		.subscribe(
			data => {
				this.teacherList = data['getTeacherRecord']; 
			},
			error => {

			}
		);   
		
	}
	
	selectTeacherModal() {
		this.selectTeacher.show();
	  }
	
	assignTeachers(){
		let selectedTeachers=[];
		let that=this;
		that.model.assignedTeachers=[];
		Object.keys(that.selectedTeachers).forEach(function(key){
			if(that.selectedTeachers[key]) selectedTeachers.push(key);
		})
		if(selectedTeachers.length <=0) return;
		that.teacherList.forEach(function(teacher){
			if(selectedTeachers.indexOf(teacher.user_id.toString()) !== -1 ){
				that.model.assignedTeachers.push(teacher);
			}
		})
		this.selectTeacher.hide();
	}
	
	deleteAssignedTeacherPopup(index:number){
		this.selectedTeacher=this.model.assignedTeachers[index];
		this.deleteTeacher.show();
	}
	
	reloadFiles(){
		// get file/folder list according to [currentFolderId] [showFileType]
	  this.dtElements.forEach((dtElement: DataTableDirective) => {
      //@ts-ignore
      if(!$(dtElement.el.nativeElement).is(':visible')) return;
      dtElement.dtInstance.then((dtInstance: DataTables.Api) => {dtInstance.ajax.reload();});
    });
	}
	//for image folders list
	traverseTo(folderId){
		this.currentFolderId=folderId;
		if(folderId != 0) this.folderList.push(folderId);
		this.reloadFiles();
	}
	
	goBack(){
		let folderId=0;
		if(this.folderList.length){
			this.folderList.splice(this.folderList.length - 1, 1);
		}
		if(this.folderList.length){
			folderId= this.folderList[this.folderList.length - 1];
		}
		this.traverseTo(folderId);
	}
	//for video folders list
	traverseToVideoFolder(folderId){
		this.currentVideoFolderId=folderId;
		if(folderId != 0) this.videoFolderList.push(folderId);
		this.reloadFiles();
	}
	
	goBackVideoFolder(){
		let folderId=0;
		if(this.videoFolderList.length){
			this.videoFolderList.splice(this.videoFolderList.length - 1, 1);
		}
		if(this.videoFolderList.length){
			folderId= this.videoFolderList[this.videoFolderList.length - 1];
		}
		this.traverseToVideoFolder(folderId);
	}
	
	removeAssignedTeacher(){
		if(this.selectedTeacher.user_id=='') return;
		this.selectedTeachers[this.selectedTeacher.user_id]=false;
		this.assignTeachers();
		this.deleteTeacher.hide();
	}
	
	instructionalLevel(event: any){
		
	}
	
	chooseFromFileLibrary(){
		this.contentModal.show();
	}
	
	chooseFromEmbeddedContent(){
		this.embeddedContentModal.show();
	}
	
	addPromoVideo(input) {  
        this.promoVideo=input.target.files[0];
        this.unsanitizedVideoUrl=window.URL.createObjectURL(this.promoVideo);
        this.model.promotion_video=this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(this.promoVideo));
        //console.log(this.promoVideo);      
    } 
    
    //for course image
    
	fileChange(input) {  
        //~ this.model.course_image=input.target.files[0];
        this.courseImage=input.target.files[0];
        this.courseImageFile=undefined;
        this.readFiles(input.target.files);      
    } 
	readFile(file, reader, callback) {  
        reader.onload = () => {  
            callback(reader.result);         
        }  
        reader.readAsDataURL(file);  
    }
    
     readFiles(files, index = 0) {  
        // Create the file reader  
        let reader = new FileReader();  
        // If there is a file  
        if (index in files) {  
            // Start reading this file  
            this.readFile(files[index], reader, (result) => {  
                // Create an img element and add the image file data to it  
                var img = document.createElement("img");  
                
                img.src = result; 
                this.course_image_srcs=(result);  
            });  
        } else {  
            // When all files are done This forces a change detection  
            this.changeDetectorRef.detectChanges();  
        }  
    }
	
	//~ imageExists(url, callback) {
	  //~ var img = new Image();
	  //~ img.onload = function() { callback(true); };
	  //~ img.onerror = function() { callback(false); };
	  //~ img.src = url;
	//~ }
	
	goToPrevious(){
		if(this.currentTab == 'advance_settings'){
			this.currentTab = 'basic_settings'
			setTimeout(_=>{
				this.initPickers();
				this.course_start_date_instance.value(this.model.start_date);
				this.course_end_date_instance.value(this.model.end_date);
			},300);
		}
	}
	
	cancel(){
		this.router.navigate(['/courses_list']);
	}
	
	selectContent(){
		this.course_image_srcs='';
		this.course_image_select.nativeElement.value="";
		this.courseImageFile=Object.assign({},this.selectedCourseImage);
		this.courseImage=this.selectedCourseImage.filename;
		this.contentModal.hide();
	}
	
	selectVideoContent(){
		this.promoVideo=Object.freeze(this.selectedPromoVideo);
		this.unsanitizedVideoUrl=Object.freeze(this.selectedPromoVideo);
    this.model.promotion_video=this.sanitizer.bypassSecurityTrustResourceUrl(this.selectedPromoVideo);
    this.videoContentModal.hide();
	}
	
	selectEmbededContent(){
		this.promoVideo=Object.freeze(this.selectedPromoVideo);
		this.unsanitizedVideoUrl=Object.freeze(this.selectedPromoVideo);
  	this.model.promotion_video=this.videoUrl(this.selectedPromoVideo);
  	this.embeddedContentModal.hide();
	}
	
	courseAssignTeachers(teachers:any){
			this.model.assignedTeachers = teachers;
			this.initPickers();
			this.course_start_date_instance.value(this.model.start_date);
			this.course_end_date_instance.value(this.model.end_date);
			this.selectedTeachers={};
			teachers.forEach(teacher=>{
				this.selectedTeachers[teacher.user_id]=true;
			});
	}
	
	
	
	addBasicInformation(){
		this.currentTab = 'advance_settings';
		let selectedTeachers=[];
		let that=this;
		Object.keys(that.selectedTeachers).forEach(function(key){
			if(that.selectedTeachers[key]) selectedTeachers.push(key);
		})
		if(selectedTeachers.length <=0) return;
		
		this.UserService.addBasicInformation(this.courseId,this.currentUser.id,selectedTeachers.join(','),this.model.course_title,this.model.sub_title,this.model.course_detail,this.courseImage,this.model.instructional_level,this.model.course_language,this.model.start_date,this.model.end_date)
			.subscribe(
					data => {
						this.courseId = data['course']['id'];
						this.model = data['course'];
						if(!this.model.course_highlights){
							this.model.course_highlights='';
						}
						if(this.model.promotion_video){
							this.unsanitizedVideoUrl=this.model.promotion_video;
							this.model.promotion_video=this.videoUrl(this.model.promotion_video);
						}
						this.courseAssignTeachers(data['teachers']);
						this.course_start_date_instance=null;
						this.course_end_date_instance=null;
					},
					error => {

					}
				);   
	}
	
	
	addAdvancedInformation(){
		//~ this.currentTab = 'payment_settings';
		this.UserService.addAdvancedInformation(this.courseId,this.model.course_completion_rule,this.promoVideo,this.model.course_highlights,this.model.keywords)
			.subscribe(
					data => {
						this.model = data['course'];
						this.router.navigate(['/courses_list']);
					},
					error => {

					}
				);    
	}
	
	
	//~ addPaymentInformation(){
			//~ this.router.navigate(['/courses_list']);
	//~ }

	

 
}
