import { Component, OnInit, DoCheck, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import {UserService} from '../_services/user.service';
import {FileLibraryService} from '../_services/file-library.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {User} from '../_models/user';
import {Params, Router,ActivatedRoute} from '@angular/router';
import * as $ from "jquery";
import {NgbRatingConfig} from '@ng-bootstrap/ng-bootstrap';
import 'jqueryui';
import { ChartType, ChartOptions } from 'chart.js';
import { SingleDataSet,MultiDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import { AppComponent } from '../app.component';
import {Calendar, Organizer} from '../../assets/js/calendarorganizer.min.js';
import * as moment from 'moment';

@Component({
    templateUrl: 'dashboard.component.html',
    providers: [NgbRatingConfig]
})

export class DashboardComponent implements OnInit, DoCheck {
currentRate=0;
@ViewChild('sortable_container', { static: false }) sortable_container: ElementRef;
language:number;
items:any;
currentUser:any;
data:any;
logo:any;
CourseList = [];
topRankedTeachers = [];
topRankedCourses = [];
teacherId:number;
instituteUsers = [];
permissions:any;
currentTab="student";
loadingComplete = false;
type:string;
id:number;
index:number;
confirmBox = false;
currentType:string;
per_page = 1;
current_page =1;
total =1;
studentId:number;
userType:string;
currentDate=moment();
organizer;
constructor(config: NgbRatingConfig,private FileLibrary: FileLibraryService, private UserService: UserService, private router: Router, private route: ActivatedRoute,private appComponent: AppComponent){
		config.max = 5;
		config.readonly = true;
	}
ngAfterViewInit(){
  let that=this;
  this.loadWidgetConfig();
  //@ts-ignore
  $(this.sortable_container.nativeElement).sortable({
    revert: true,
    handle: ".handle",
    stop: function( event, ui ) {
      that.savePositions();
    }
  });
  let calendar = new Calendar("calendarContainer", "small", [ "Monday", 1 ], [ "#74cacb", "#1c7b8f", "#ffffff", "#ffffff" ]);
  this.organizer = new Organizer("organizerContainer", calendar);
  this.organizer.setOnClickListener('day-slider', function () { 
    that.currentDate=moment(that.currentDate).subtract(1,'days');
    that.loadEvents();
  }, function () {
    that.currentDate=moment(that.currentDate).add(1,'days');
    that.loadEvents();
  });
  this.organizer.setOnClickListener('days-blocks', function (date) {
    that.currentDate=moment(date);
    that.loadEvents();
  }, null);
  this.organizer.setOnClickListener('month-slider', function () {
    that.currentDate=moment(that.currentDate).subtract(1,'month');
    that.loadEvents();
  }, function () {
    that.currentDate=moment(that.currentDate).add(1,'month');
    that.loadEvents();
  });
  this.organizer.setOnClickListener('year-slider', function () {
    that.currentDate=moment(that.currentDate).subtract(1,'year');
    that.loadEvents();
  }, function () {
    that.currentDate=moment(that.currentDate).add(1,'year');
    that.loadEvents();
  });
  this.loadEvents();
}
ngOnInit() {

	this.data=JSON.parse(localStorage.getItem("language"));
	this.language = JSON.parse(localStorage.getItem("activeLanguage"));
	this.currentUser= JSON.parse(localStorage.getItem("user_data"));
	let config=localStorage.getItem('dashboard-widget-config');
  if(config){
    this.widgetConfig=JSON.parse(config);
  }

  this.getSiteStats();
	this.getStorageStats();
	this.getEnrollStats();
	this.getCompletedClassHoursStats();
	this.getTotalCoursesStats();
	this.getNumberOfLiveClassStats();
	this.getTeachersStats();
	this.getTopRankedCourses();
}

ngDoCheck(){
	if(JSON.parse(localStorage.getItem("activeLanguage")) != this.language){
	 this.language=JSON.parse(localStorage.getItem("activeLanguage"));
	}
}

//Users Details Pie Charts
  public userDetailsOptions: ChartOptions = {
    responsive: true,
  };
  public site_visitors=0;
  public liveClassUsersLabels: Label[] = ["In Live Class", "Total"];
  public liveClassRunningLabels: Label[] = ["Live Classes", "Total"];
  public liveClassUsersData: SingleDataSet = [0, 0];
  public liveClassRunningData: SingleDataSet = [0, 0];
  public userDetailsType: ChartType = 'pie';
  public userDetailsLegend = false;
  public userDetailsPlugins = [];
  public userDetailsColor = [
  {
    backgroundColor: ["#74CACB","#1C7B8F"],
  },
  // ...colors for additional data sets
  
  ];

//Widgets Chart Groups Pie
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: { position: 'bottom',
				display: true,
				labels: {
					boxWidth: 8,
					usePointStyle: true,
	
				},
			},
	title:{
			display: true,
			//text: 'Total Enrollment',
			fontColor:'#49B1B4',
			position:'top',
			fontSize:18,
		  },
    
  };

//Chart total-enrollments

  public totalEnrollmentLabels: Label[] = ["Active", "InActive"];
  public totalEnrollmentData: SingleDataSet = [0,0];
  public totalEnrollmentType: ChartType = 'pie';
  public totalEnrollmentLegend = true;
  public totalEnrollmentPlugins = [];
  public totalEnrollmentColor= [
	{
		backgroundColor: ["#639CD3", "#F390A4"],
	},
  ];
  
//Chart number-of-class

  public numberOfLiveClassLabels: Label[] = ["Past", "Active", "Upcoming"];
  public numberOfLiveClassData: SingleDataSet = [0,0,0];
  public numberOfLiveClassType: ChartType = 'pie';
  public numberOfLiveClassLegend = true;
  public numberOfLiveClassPlugins = [];
  public numberOfLiveClassColor= [
	{
		backgroundColor: ["#639CD3", "#F390A4", "#5EBA4B"],
	},
  ];
 
//Chart total-courses 
  
  public totalCoursesLabels: Label[] = ["Active", "Completed", "Upcoming"];
  public totalCoursesData: SingleDataSet = [0,0,0];
  public totalCoursesType: ChartType = 'pie';
  public totalCoursesLegend = true;
  public totalCoursesPlugins = [];
  public totalCoursesColor= [
	{
		backgroundColor: ["#639CD3", "#F390A4", "#5EBA4B"],
	},
  ];
  
  
// Chart storage-gb	 
  
  public storageLabels: Label[] = [];
  public storageData = [{
        label: "Usage",
        backgroundColor: "#629cd3",
        borderColor: "#3c8fed",
        hoverBackgroundColor: "#3c8fed",
        hoverBorderColor: "#629cd3",
        hoverBorderWidth: 0,
        data: [],
        borderWidth: 0,
        stack: 'a'
      },
      {
        label: "Limit",
        backgroundColor: "#f390a4",
        hoverBackgroundColor: "#f35c7c",
        borderColor: "#f35c7c",
        hoverBorderColor: "#f390a4",
        data: [],
        hoverBorderWidth: 0,
        borderWidth: 0,
        stack: 'a'
      }];
  public storageType: ChartType = 'horizontalBar';
  public storagePlugins = [];
  public storageOptions: ChartOptions = {
    responsive: true,
	legend: { position: 'bottom',
			  display: true,
			  labels: {
				boxWidth: 8,
				usePointStyle: true,
				},
			},
	title:{
			display: true,
			//text: 'Total Enrollment',
			fontColor:'#49B1B4',
			position:'top',
			fontSize:18,
		  },
	scales:{
			xAxes: [{
				gridLines: {
					display:false
				},
				ticks: {
					beginAtZero: true
				}
			}],
			yAxes: [{
				gridLines: {
					display:false
				}
			}]
		}
    
  };
  
// Chart completed-live-class 
  
  public completedLiveClassLabels: Label[] = [];
  public completedLiveClassData = [{
          label: "Usage",
          backgroundColor: "#629cd3",
          borderColor: "#3c8fed",
          hoverBackgroundColor: "#3c8fed",
          hoverBorderColor: "#629cd3",
          hoverBorderWidth: 0,
          data: [],
          borderWidth: 0
        }];
  public completedLiveClassType: ChartType = 'horizontalBar';
  public completedLiveClassPlugins = [];
  public completedLiveClassOptions: ChartOptions = {
    responsive: true,
    legend: {   display: false,
				labels: {
					boxWidth: 8,
					usePointStyle: true,
	
				},
			},
	title:{
			display: true,
			fontColor:'#49B1B4',
			position:'top',
			fontSize:18,
		  },
	scales:{
			xAxes: [{
				gridLines: {
					display:false
				},
				ticks: {
					beginAtZero: true
				}
			}],
			yAxes: [{
				gridLines: {
					display:false
				}
			}]
		}
    
  };
public currentWidget={name: '',position: 0};
public widgetConfig={
  widgets: [
    {show: true,deleted: false},
    {show: true,deleted: false},
    {show: true,deleted: false},
    {show: true,deleted: false},
    {show: true,deleted: false},
    {show: true,deleted: false},
    {show: true,deleted: false}
  ],
  positions: [0,1,2,3,4,5,6]
};
refreshWidgets(){
  this.widgetConfig.positions=[0,1,2,3,4,5,6];
  this.widgetConfig.positions.forEach((position)=>{
    this.widgetConfig.widgets[position]={show: true,deleted: false};
  });
  this.saveConfig();
  this.loadWidgetConfig();
}
loadWidgetConfig(){
  this.widgetConfig.positions.forEach((position, index)=>{
    $('[data-position="'+position.toString()+'"]').insertAfter($(this.sortable_container.nativeElement).children(':nth-child('+(index+1).toString()+')'))
  });
}
saveConfig(){
  localStorage.setItem('dashboard-widget-config', JSON.stringify(this.widgetConfig));
}
toggleWidget(position){
  this.widgetConfig.widgets[position].show=!this.widgetConfig.widgets[position].show;
  this.saveConfig();
}
deleteWidget(widget){
  this.widgetConfig.widgets[widget.position].deleted=true;
  this.saveConfig();
}
savePositions(){
  let positions=[];
  $('[data-position]').each(function(){
    positions.push(parseInt($(this).data('position')));
  });
  this.widgetConfig.positions=positions;
  this.saveConfig();
}
getStorageStats(){
	this.FileLibrary.getAllSpaceUsage().subscribe(
    resp => {
      let usedSpace=[];
      let limitSpace=[];
      let labels=[];
      resp.data.teachers.forEach(detail=>{
        labels.push(detail.teacher.firstname+' '+detail.teacher.lastname);
        usedSpace.push(detail.used_space.toFixed(2));
        limitSpace.push(detail.teacher.storage);
      })
      
      this.storageLabels=labels;
      this.storageData = [{
  			label: "Usage",
  			backgroundColor: "#629cd3",
  			borderColor: "#3c8fed",
  			hoverBackgroundColor: "#3c8fed",
  			hoverBorderColor: "#629cd3",
  			hoverBorderWidth: 0,
  			data: usedSpace,
  			borderWidth: 0,
        stack: 'a'
  		},
  		{
  			label: "Limit",
  			backgroundColor: "#f390a4",
  			hoverBackgroundColor: "#f35c7c",
  			borderColor: "#f35c7c",
  			hoverBorderColor: "#f390a4",
  			data: limitSpace,
  			hoverBorderWidth: 0,
  			borderWidth: 0,
        stack: 'a' 
  		}];
    });
}
loadEvents(){
  this.UserService.getEventsForDate(moment(this.currentDate).format('YYYY-MM-DD'))
  .subscribe(
    data => { 
      this.organizer.list(data); 
    },
    error => {

    }
  );
}
getSiteStats(){
	this.liveClassUsersData=[0,0];
	this.liveClassRunningData=[0,0];
	this.site_visitors=0;
	this.UserService.siteStats()
	.subscribe(
		data => { 
			this.liveClassUsersData = [data.total_users_in_class,data.total_users];	
			this.liveClassRunningData= [data.total_active_classes, data.total_classes];
			this.site_visitors=data.total_visitors;
		},
		error => {

		}
	); 
}

getEnrollStats(){
	this.totalEnrollmentData=[0,0];
	this.UserService.getEnrollStats()
	.subscribe(
		resp => { 
			this.totalEnrollmentData = [resp.activeEnrollments,resp.InActiveEnrollments];
		},
		error => {

		}
	); 
}

getTotalCoursesStats(){
	this.UserService.getTotalCoursesStats()
	.subscribe(
		resp => { 
			this.totalCoursesData=[resp.activeCourses,resp.completedCourses,resp.upcomingCourses];
		},
		error => {

		}
	); 
}

getTeachersStats(){
	this.UserService.getTeachersStats()
	.subscribe(
		resp => { 
			this.topRankedTeachers = resp.topRankedTeachers;
		},
		error => {

		}
	); 
}

getNumberOfLiveClassStats(){
	this.UserService.getNumberOfLiveClassStats()
	.subscribe(
		resp => { 
			this.numberOfLiveClassData = [resp.completedClasses,resp.activeClasses,resp.upcomingClasses];
		},
		error => {

		}
	); 
}
getTopRankedCourses(){
	this.UserService.getTopRankedCourses()
	.subscribe(
		resp => { 
			this.topRankedCourses=resp.topRankedCourses;
		},
		error => {

		}
	); 
}

getCompletedClassHoursStats(){
	this.UserService.getCompletedClassHoursStats()
	.subscribe(
		resp => { 
			let labels=[];
			let completedHours=[];
			resp.completedHours.forEach(detail=>{
				//~ console.log(detail);
			  labels.push(detail.teacher.firstname+' '+detail.teacher.lastname);
			  completedHours.push((detail.sum)/60);
			});
			this.completedLiveClassLabels=labels;
			this.completedLiveClassData=[{
					label: "Usage",
					backgroundColor: "#629cd3",
					borderColor: "#3c8fed",
					hoverBackgroundColor: "#3c8fed",
					hoverBorderColor: "#629cd3",
					hoverBorderWidth: 0,
					data: completedHours,
					borderWidth: 0
				}];
		},
		error => {

		}
	); 
}

getInstituteUser(type:string, page:number){
		this.UserService.getInstituteUser(type, page)
	.subscribe(
				data => { 
				this.instituteUsers = data['getInstituteUser'].data;	
				this.current_page = data['getInstituteUser']['current_page'];		
				this.per_page = data['getInstituteUser']['per_page'];		
				this.total = data['getInstituteUser']['total'];	
				if(type !== this.currentTab)
				 this.currentTab=type;
				},
				error => {

				}
			); 
}
deleteInstituteUser(id:number, index:number){
	this.UserService.deleteInstituteUser(id)
	.subscribe(
			data => {
				this.instituteUsers.splice(index,1);
			}
		);
}
 editInstituteUser(userId:number){
	$('.edit_institute_'+userId).show();
}
 
cancelInstituteUser(userId:number){
	$('.edit_institute_'+userId).hide();
}

saveInstituteUser(userId:number){
	var name = $("#name_"+userId).val();
	alert(userId);
	this.UserService.saveInstituteUser(userId, name)
	.subscribe(
			data => { 
				if(data['code'] == 200){
					$('.edit_institute_'+userId).hide();
					alert(this.currentTab);
					this.getInstituteUser(this.currentTab, 1);
				}			
			
			},
			error => {

			}
		); 
}


public InstituteUser(id:number, index:number){
	this.id =  id;
	this.index = index;
	this.confirmBox=true;
}

closePopUp(){		
	this.confirmBox=false;
}

public deleteUser(){
	this.confirmBox=false;
	this.deleteInstituteUser(this.id, this.index);
}


assign_course(teacherId,userType){
	this.teacherId = teacherId;
	this.studentId = teacherId;
	this.userType = userType;
	this.getCourses('s');
}


getCourses(user_type:string){
	   this.UserService.getCourses(this.userType,this.teacherId)
			.subscribe(
				data => { 
						this.CourseList = data;
				},
				error => {

				}
			); 
	   
}

	assignCourses(){
		this.appComponent.isLoading = true;
		var val = [];
		this.UserService.assignCourses(this.teacherId,$('[name^="courseList1"]').serialize())
				.subscribe(
					data => {
						this.appComponent.isLoading = false;
						this.loadingComplete = true;
						$("#DeleteMessage").text(data['message']).delay(5000).fadeOut();
					},
					error => {
						this.appComponent.isLoading = false;
						this.loadingComplete = true;
					}
				);
		}
	
	SaveToEnroll(){
		this.appComponent.isLoading = true;
		console.log($('[name^="courseList"]').serialize());
		this.UserService.SaveToEnroll(this.studentId,this.language,$('[name^="courseList"]').serialize())
		.subscribe(
				data => {
						this.appComponent.isLoading = false;
						this.loadingComplete = true;
						$("#DeleteMessage").text(data['message']).delay(5000).fadeOut()	
					 
				},
				error => {
						this.appComponent.isLoading = false;
						this.loadingComplete = true;
				}
			); 
	}


}
