import { Component, OnInit, DoCheck, Inject } from '@angular/core';
import {HttpEventType, HttpResponse} from '@angular/common/http';
import {UserService} from '../_services/user.service';
import {User} from '../_models/user';
import {Params, Router} from '@angular/router';
import * as $ from "jquery";
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';



@Component({
   //moduleId: module.id,
  templateUrl: './logo-settings.component.html'
})

export class LogoSettingComponent implements OnInit,DoCheck {
  operation={
    'web_logo': {lifecycle: 'started',status: false,message: '',progress: 0},
    'class_logo': {lifecycle: 'started',status: false,message: '',progress: 0},
    'fav_icon': {lifecycle: 'started',status: false,message: '',progress: 0}
  };
  model:any={left_title:'',left_description:'',right_title:'',right_description:'',copyright_title:''};
  public web_logo= '';
  public class_logo= '';
  public fav_icon= '';

  public web_logo_upload={name:''};
  public class_logo_upload={name:''};
  public fav_icon_upload={name:''};
  
  currentUser:any={};

  constructor(private UserService: UserService, private router: Router,private sanitized: DomSanitizer) { }
     
  ngOnInit() {
    this.currentUser= JSON.parse(localStorage.getItem("user_data"));
    this.getLoginSettings()
  }

  fileChange(input,logo) {  
      this[logo+'_upload']=input.target.files[0];
      this.readFiles(input.target.files,logo);      
  } 
  readFile(file, reader, callback) {  
      reader.onload = () => {  
          callback(reader.result);         
      }  
      reader.readAsDataURL(file);  
  }
    
  readFiles(files,logo) {
    let index=0;  
    // Create the file reader  
    let reader = new FileReader();  
    // If there is a file  
    if (index in files) {  
      // Start reading this file  
      this.readFile(files[index], reader, (result) => {  
          // Create an img element and add the image file data to it  
          var img = document.createElement("img");  
          
          img.src = result;
          this[logo]=this.sanitized.bypassSecurityTrustUrl(result);  
      });  
    }  
  }    

  getWidthPercent(percentDone){
    return this.sanitized.bypassSecurityTrustStyle('width: '+percentDone+'%');
  }
  resetSettings(logo){
    this.operation[logo]={lifecycle: 'running',status: false,message: ''};
  
    this.UserService.resetLoginSettings(logo)
   .subscribe(
      resp => {
        this.operation[logo]={lifecycle: 'stopped',status: true,message: 'Logo reset successfully'};
        this.operation[logo]={lifecycle: 'stopped',status: true,message: 'Settings reset successfully'};
        this.setSettings(resp.data);
      },
      error => {
        this.operation[logo]={lifecycle: 'stopped',status: false,message: 'Problem resetting Logo'};
      }
    );
  }
  saveLoginSettings(logo){
    this.operation[logo]={lifecycle: 'running',status: false,message: '',progress: 0};
    this.UserService.saveLogoSettings(logo,this[logo+'_upload'],{reportProgress: true, observe: 'events' })
     .subscribe(
        event => {
          if (event.type === HttpEventType.UploadProgress) {
            let percentDone = Math.round(100 * event.loaded / event.total);
            this.operation[logo]['progress']=percentDone;
          } else if (event instanceof HttpResponse) {
            this.getLoginSettings();
            this.operation[logo]={lifecycle: 'stopped',status: true,message: 'Logo updated successfully',progress: 100};
            setTimeout(_=>{
              this.operation[logo]={lifecycle: 'started',status: false,message: '',progress: 0};
            },2000)
          }
        },
        error => {
          this.operation[logo]={lifecycle: 'stopped',status: false,message: 'Problem saving logo',progress: 0};
          setTimeout(_=>{
            this.operation[logo]={lifecycle: 'started',status: false,message: '',progress: 0};
          },2000)
        }
      );
  }
  setSettings(settings){
    if(settings && !!settings['web_logo']){
      this.web_logo=settings['web_logo'];
    }
    if(settings && !!settings['class_logo']){
      this.class_logo=settings['class_logo'];
    }
    if(settings && !!settings['fav_icon']){
      this.fav_icon=settings['fav_icon'];
    }
    if(settings){
      this.UserService.updateSiteSettings(settings);
    }
  }
  getLoginSettings(){
    this.UserService.getLoginSettings()
    .subscribe(
      data => { 
        this.setSettings(data['getLoginSetting']);
      },
      error => {

      }
    );
  
  }
  ngDoCheck(){
    
	}
 }

    

			

																																																																																																																								

