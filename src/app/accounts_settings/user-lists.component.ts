import { Component, OnInit, DoCheck, Inject, ViewChild, ElementRef } from '@angular/core';
import {UserService} from '../_services/user.service';
import {User} from '../_models/user';
import {Params, Router,ActivatedRoute} from '@angular/router';
import { ModalDirective } from 'angular-bootstrap-md';
import { DataTableDirective } from 'angular-datatables';


@Component({
   //moduleId: module.id,
  templateUrl: './user-lists.component.html'
})

export class UserListComponent implements OnInit,DoCheck {
	@ViewChild('addUserModal', { static: true }) addUserModal: ModalDirective;
	@ViewChild('bulkUserModal', { static: true }) bulkUserModal: ModalDirective;
	@ViewChild('upload_csv', { static: false }) upload_csv: ElementRef;

	@ViewChild(DataTableDirective, {static: false})
	private datatableElement: DataTableDirective;
	dtOptions: any = {}
	operation={lifecycle: 'started',status: false,message: ''};
  commonOperation={lifecycle: 'started',status: false,message: ''};
  model: any = {};
   	usersList = [];
   	userTypeList = [];
   	totallength:number;
   	inactive:string = "In Active";
   	active:string = "Active";
   	validEmail:boolean;
   	instituteUser:string="";
   	currentUser:any;
   	docFile:any=null;
   	errorMsg: any={};

  constructor(private router: Router, private UserService: UserService,private route: ActivatedRoute) {    		
    }
     
   //~ ngOnInit() {
		//~ this.currentUser= JSON.parse(localStorage.getItem("user_data"));
		//~ this.route.params.subscribe(params => {this.instituteUser=params['user']});
		//~ this.getUserList();
		//~ this.getUserType();	
	ngOnInit() {
		const that = this;
		that.currentUser= JSON.parse(localStorage.getItem("user_data"));
		that.route.params.subscribe(params => {this.instituteUser=params['user']});
		that.getUserType();
		that.dtOptions = {
						pagingType: 'simple_numbers',
						pageLength: 10,
						language: {
							search: "_INPUT_",
							searchPlaceholder: "Search by User Name"
						},
						bPaginate: true,
						bLengthChange: true,
						bInfo: true,
						bAutoWidth: false,
						processing: true,
						serverSide: true,
						dom: 'lBfrtip',
							buttons: [
							{
								extend:    '',
								text:      '<i class="fa fa-share-square-o" aria-hidden="true"></i>'
							},
							{
								extend:    'csvHtml5',
								titleAttr: 'CSV',
								action: function (e, dt, node, config) {
									let userToken=localStorage.getItem('token');
			            window.location.href=that.UserService.exportAll('csv',userToken);
			          }
							},
							{
								extend:    'excelHtml5',
								titleAttr: 'Excel',
								action: function (e, dt, node, config) {
									let userToken=localStorage.getItem('token');
			            window.location.href=that.UserService.exportAll('xls',userToken);
			          }
							},
							{
								extend:    'pdfHtml5',
								titleAttr: 'PDF',
								action: function (e, dt, node, config) {
									let userToken=localStorage.getItem('token');
			            window.location.href=that.UserService.exportAll('pdf',userToken);
			          }
							}
						],
						ajax: (dataTablesParameters: any, callback) => {
							that.UserService.getAllUsers(dataTablesParameters).subscribe(resp => {
								that.usersList = resp.user_data.data;
								callback({
								  recordsTotal: resp.user_data.total,
								  recordsFiltered: resp.user_data.total,
								  data: []
								});
							});
						},
						columns: [{ data: 'email' },{ data: 'firstname' }, {'searchable': false,'orderable': false },{ data: 'user_type' }, { 'searchable': false,data: 'is_active' }, { 'searchable': false,'orderable': false }],
						order: [[ 0, "asc" ]]
					};
	}
  
	ngDoCheck(){
		
	}
	


	 onChange(newValue) {
		const validEmailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if (validEmailRegEx.test(newValue)) {
			this.validEmail = true;
		}else {
			this.validEmail = false;
		}

	}
	
	
    onClosed(e,form){
    	this.model={};
    	this.errorMsg={};
    	if(form) form.submitted=false;
    }
    addInstituteUser(){
			this.UserService.addInstituteUser(this.model.firstname,this.model.lastname, this.model.email, this.model.password,this.currentUser['institute_id'],$('#user_type :selected').attr('id'))
			.subscribe(
				data => {
					if(data['code']==200){
						this.addUserModal.hide();
						this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
							dtInstance.ajax.reload();
						});
					}
					this.errorMsg['addUser']=data.message;
				},error=>{
					this.errorMsg['addUser']=error.message;
				});

	}
    resetCommonOps(){
    	this.commonOperation={lifecycle: 'started',status: true,message: ''};
  	}
    getValueOfSwitches(event: any, userId:number){
		var status;
		if(event.target.checked){
			status = 2;
		}else{
			status = 0;
		}
		this.commonOperation={lifecycle: 'running',status: false,message: (status ? 'Activating' : 'Deactivating')+' user, please wait..'};
  	
		this.UserService.changeStatus(status,userId)
				.subscribe(
					data => {
						this.commonOperation={lifecycle: 'stopped',status: true,message: 'User has been '+(status ? 'activated' : 'deactivated')};
  					this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
							dtInstance.ajax.reload();
						}); 
						setTimeout(_=>{
							this.resetCommonOps();
						},3000);
					},
					error => {
						this.commonOperation={lifecycle: 'stopped',status: false,message: error.message};
  					this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
							dtInstance.ajax.reload();
						});

						setTimeout(_=>{
							this.resetCommonOps();
						},3000);
					}
				);   
	}
	autoGenPass(){
		if(!this.model.auto_password){
			this.model.password='';
		}
	}
	deleteUser(userId:number,index:number){
		this.UserService.deleteUser(userId)
				.subscribe(
					data => { 
						this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
							dtInstance.ajax.reload();
						});
					},
					error => {

					}
				);   
	} 
	
	getUserType(){
		this.UserService.getUserType()
				.subscribe(
					data => { 
						this.userTypeList = data;
						
					},
					error => {

					}
				);   

	}
	
	fileChange(input){
	  if(input.target.files.length){
	  	this.docFile=input.target.files[0];	
	  }
  } 
  clearOpeartion(){
  	this.operation={lifecycle: 'started',status: false,message: ''};
  	this.docFile=null;
  	this.upload_csv.nativeElement.value='';
  }
  submitCSV(){
  	if(!this.docFile) return;

    this.operation={lifecycle: 'running',status: false,message: 'Importing users. Please wait..'};
  	this.UserService.submitCSV(this.docFile)
		.subscribe(  
			data => {
				 this.operation={lifecycle: 'stopped',status: true,message: 'Users imported successfully'};
      	 this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
						dtInstance.ajax.reload();
					});
				 	setTimeout(()=>{
	          this.bulkUserModal.hide();
	        },1500)  
			},
			error => {
				this.operation={lifecycle: 'stopped',status: false,message: 'Users imported with some errors: <br> <ul>'+error.message.join('</li><li>')+'</ul>'};
      }
		);	
	}
	
	
 }


    

			

																																																																																																																								

