import { Component, OnInit, DoCheck, Inject, ViewChild } from '@angular/core';
import {EmailTemplateService} from '../_services/email-template.service';
import { EmailTemplate } from '../_models/email_template';
import {Params, Router,ActivatedRoute} from '@angular/router';
import { ModalDirective } from 'angular-bootstrap-md';
import { DataTableDirective } from 'angular-datatables';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';


@Component({
   //moduleId: module.id,
  templateUrl: './email_templates.component.html'
})

export class EmailTemplateComponent implements OnInit,DoCheck {
  @ViewChild('editTemplateModal', { static: true }) editTemplateModal: ModalDirective;
  operation={lifecycle: 'started',status: false,message: ''};
  selectedTemplate:EmailTemplate=new EmailTemplate();
  templates={};
  currentTab='user';
  Editor = ClassicEditor;
     
  constructor(private router: Router, private EmailTemplateService: EmailTemplateService,private route: ActivatedRoute) {       
  }
  ngOnInit() {
    this.EmailTemplateService.getAllTemplates().subscribe(resp=>{
      resp.forEach(template=>{
        this.templates[template.type]=template;
      });
    },error=>{
      //Todo: do something with error
    })  
    
  }
  ngDoCheck(){
    
  }
  resetSelected(){
    this.selectedTemplate=new EmailTemplate();  
  }
  saveTemplate(template,mdl){
    this.operation={lifecycle: 'running',status: false,message: ''};
    this.EmailTemplateService.saveTemplate(template)
      .subscribe(
        data => {
          this.operation={lifecycle: 'stopped',status: true,message: 'Template saved successfully'};
          setTimeout(_=>{
            /* Not Sure why model is giving undefined in production mode, for now passing it as argument
              TODO: fix this bug if found
            */
            if(mdl) mdl.hide();
            this.operation={lifecycle: 'started',status: false,message: ''}
          },3000);
        },
        error => {
          this.operation={lifecycle: 'stopped',status: false,message: error.message};
          setTimeout(_=>{
            this.operation={lifecycle: 'started',status: false,message: ''}
          },3000);
        }
      );
  }
  
 }