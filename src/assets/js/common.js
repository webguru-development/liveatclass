
$(document).ready(function() {
	$( function() {
		$( "#sortable" ).sortable({
		  revert: true
		});
    	$( ".chart-groups" ).disableSelection();
	  } );
	  
	$("#referesh-page").click(function(){
		location.reload(true);
	});
	
	var hash = document.location.hash;
		var prefix = "tab_";
		if (hash) {
			$('.nav-tabs a[href="'+hash.replace(prefix,"")+'"]').tab('show');
		} 
		
		// Change hash for page-reload
		$('.nav-tabs a').on('shown', function (e) {
			window.location.hash = e.target.hash.replace("#", "#" + prefix);
		});

    ClassicEditor
        .create( document.querySelector( '#editor1' ) )
        .then(editor=>{
          editor.ui.focusTracker.on( 'change:focusedElement', ( evt, name, value) => {
              if(value){
                $(editor.ui.view.element).addClass('editor-focused');
              }else{
                $(editor.ui.view.element).removeClass('editor-focused');
              }
          });
        })
        .catch( error => {
            console.error( error );
        } );
    ClassicEditor
        .create( document.querySelector( '#editor2' ) )
        .then(editor=>{
          editor.ui.focusTracker.on( 'change:focusedElement', ( evt, name, value) => {
              if(value){
                $(editor.ui.view.element).addClass('editor-focused');
              }else{
                $(editor.ui.view.element).removeClass('editor-focused');
              }
          });
        }).catch( error => {
            console.error( error );
        } );
	
	new Chart(document.getElementById("pie-chart1"), {
			type: 'pie',
			data: {
			  //labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
			  datasets: [{
				//label: "Population (millions)",
				backgroundColor: ["#74CACB","#1C7B8F"],
				data: [133,784]
			  }]
			}//,
//			options: {
//			  title: {
//				display: true,
//				text: 'Predicted world population (millions) in 2050'
//			  }
//			}
		});
		new Chart(document.getElementById("pie-chart2"), {
			type: 'pie',
			data: {
			  datasets: [{
				backgroundColor: ["#74CACB","#1C7B8F"],
				data: [133,784]
			  }]
			}
		});
		
		// Chart storage-gb		
		new Chart(document.getElementById("storage-gb"), {
			type: 'horizontalBar',
			data: {
			  labels: ["User 1", "User 2", "User 3"],
			  datasets: [
				{
				  label: "Usage",
				  backgroundColor: "#7CB5EC",
				  data: [407,631,635],
						borderWidth: 0
				}, {
				  label: "Limit",
				  backgroundColor: "#F491A3",
				  data: [814,841,1114],
						borderWidth: 0
				}
			  ]
			},
			options: {
				legend: {
					display: true,
					labels: {
						display: true,
						boxWidth: 8,
						usePointStyle: true,
					},
					position:'bottom'
				},
			  title: {
				display: true,
				//text: 'User Data Storage in GB',
				fontColor:'#49B1B4',
				position:'top',
				fontSize:'18',
			  },
			  scales: {
				xAxes: [{
					gridLines: {
						display:false
					},
					ticks: {
						beginAtZero: true
					}
				}],
				yAxes: [{
					gridLines: {
						display:false
					}
				}]
			}
			}
		});
		
		//Chart total-enrollments
		new Chart(document.getElementById("total-enrollments"), {
			type: 'pie',
			data: {
			  labels: ["Past", "Active"],
			  datasets: [{
				label: "Population (millions)",
				backgroundColor: ["#639CD3", "#F390A4"],
				data: [8500,1500]
			  }]
			},
			options: {
				legend: {
					display: true,
					labels: {
						display: true,
						boxWidth: 8,
						usePointStyle: true,
		
					},
					position:'bottom'
				},
			  title: {
				display: true,
				//text: 'Total Enrollment',
				fontColor:'#49B1B4',
				position:'top',
				fontSize:'18',
			  },
			}
		});
		
		// Chart completed-live-class		
		new Chart(document.getElementById("completed-live-class"), {
			type: 'horizontalBar',
			data: {
			  labels: ["Teacher 1", "Teacher 2", "Teacher 3", "Teacher 4", "Teacher 5" ],
			  datasets: [
				{
				  label: "Usage",
				  backgroundColor: "#7CB5EC",
				  data: [82,96,105,110,120],
						borderWidth: 0
				}
			  ]
			},
			options: {
				legend: {
					display: false,
					labels: {
						display: true,
						boxWidth: 8,
						usePointStyle: true,
		
					}
				},
			  title: {
				display: true,
				//text: 'User Data Storage in GB',
				fontColor:'#49B1B4',
				position:'top',
				fontSize:'18',
			  },
			  scales: {
				xAxes: [{
					gridLines: {
						display:false
					},
					ticks: {
						beginAtZero: true
					}
				}],
				yAxes: [{
					gridLines: {
						display:false
					}
				}]
			}
			}
		});
		
		//Chart number-of-class
		new Chart(document.getElementById("number-of-class"), {
			type: 'pie',
			data: {
			  labels: ["Past", "Active", "Upcoming"],
			  datasets: [{
				label: "Population (millions)",
				backgroundColor: ["#639CD3", "#F390A4", "#5EBA4B"],
				data: [35,55,55]
			  }]
			},
			options: {
				legend: {
					display: true,
					labels: {
						display: true,
						boxWidth: 8,
						usePointStyle: true,
		
					},
					position:'bottom'
				},
			  title: {
				display: true,
				//text: 'Total Enrollment',
				fontColor:'#49B1B4',
				position:'top',
				fontSize:'18',
			  },
			}
		});
		
		//Chart total-courses
		new Chart(document.getElementById("total-courses"), {
			type: 'pie',
			data: {
			  labels: ["Active", "Completed", "Upcoming"],
			  datasets: [{
				label: "Population (millions)",
				backgroundColor: ["#639CD3", "#F390A4", "#5EBA4B"],
				data: [30,70,100]
			  }]
			},
			options: {
				legend: {
					display: true,
					labels: {
						display: true,
						boxWidth: 8,
						usePointStyle: true,
		
					},
					position:'bottom'
				},
			  title: {
				display: true,
				//text: 'Total Enrollment',
				fontColor:'#49B1B4',
				position:'top',
				fontSize:'18',
			  },
			}
		});
		
		//~ $('.toggle-icon a').click(function(){
            //~ $('#sidebar').toggleClass('togglesidebar');
            //~ $('#main-block').toggleClass('togglemaincontent');
            //~ $('#change-icon').toggleClass('active');
        //~ });
        
        // Draggable 
		
		$('#exampleModal1').draggable().addClass("draggable-cursor");
		$('#exampleModal4').draggable().addClass("draggable-cursor");

	
});	
 
