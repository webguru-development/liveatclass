'use strict'
const CONFIG=require('./config');
// const MYSQL=require('mysql')
const REQUEST = require('request-promise')
// const CONN = MYSQL.createConnection({
//   host     : CONFIG.MYSQL_HOST,
//   port     : CONFIG.MYSQL_PORT,
//   user     : CONFIG.MYSQL_USER,
//   password : CONFIG.MYSQL_PWD,
//   database : CONFIG.MYSQL_DB
// });
// const FS= require('fs');
// const PDFImage = require("pdf-image").PDFImage;
const path = require("path");
var Utility = function () {
  function initUtilities (redis, socket, io, mapSocketToDiscussion, user, loggedInUser, token) {
    function loadWorkspace(workplace, withOutChat){
      if (!workplace['tabs']) workplace['tabs'] = []
      if (!workplace['chat']) workplace['chat'] = {}
      if (!workplace['whiteboard']) workplace['whiteboard'] = {}
      if (!workplace['settings']) workplace['settings'] = []
      if (!workplace['layout']) workplace['layout'] = 'classroom-view'
      // layout change
      socket.emit('layout',workplace['layout']);
        
      // dump tabs
      for (let i = 0; i < workplace['tabs'].length; i++) {
        socket.emit('tabadd', JSON.stringify(workplace['tabs'][i]))
      }

      // making chat optional for template
      if(!withOutChat){
        // dump chat
        Object.keys(workplace['chat']).forEach(group=>{
          if(group.indexOf(user.identity) !== -1 || group=='common'){
            socket.emit('chatgroup', JSON.stringify(workplace['chat'][group]));
          }
        })
      }
      
      // give time to create tabs
      setTimeout(function () {
        socket.emit('tabchanged',workplace['currentTab']);
        // dump whiteboard drawings
        if(workplace['whiteboard']) Object.keys(workplace['whiteboard']).forEach(tabId=>{
          if(workplace['whiteboard'][tabId]){
            workplace['whiteboard'][tabId]['drawings'].forEach(drawing=>{
              socket.emit('drawing', drawing, tabId, 'whiteboard', 0, true);
            });
            socket.emit('move', workplace['whiteboard'][tabId]['currentPos'], tabId);
            socket.emit('zoom', workplace['whiteboard'][tabId]['zoom'], tabId);
            socket.emit('grid', workplace['whiteboard'][tabId]['grid'], tabId);
          }
        })
        // dump content drawings
        if(workplace['content']) Object.keys(workplace['content']).forEach(tabId=>{
          if(workplace['content'][tabId]){
            Object.keys(workplace['content'][tabId]['pages']).forEach(pageId=>{
              if(workplace['content'][tabId]['pages'][pageId]){
                workplace['content'][tabId]['pages'][pageId]['drawings'].forEach(drawing=>{
                  socket.emit('drawing', drawing, tabId, 'content' , pageId, true);
                });
                socket.emit('move', workplace['content'][tabId]['pages'][pageId]['currentPos'], tabId);
                socket.emit('zoom', workplace['content'][tabId]['pages'][pageId]['zoom'], tabId);
                socket.emit('grid', workplace['content'][tabId]['pages'][pageId]['grid'], tabId);
              }
            });
            socket.emit('changepage', tabId, workplace['content'][tabId]['currentPage'], true);
          }
        })
        
        let tabIds = Object.keys(workplace['settings'])
        for (let i = 0; i < tabIds.length; i++) {
          let tab = workplace['settings'][tabIds[i]]
          for (let j = 0; j < tab['ops'].length; j++) {
            socket.emit(tab['ops'][j][0], tab['ops'][j][1], tabIds[i])
          }
        }
        socket.emit('dumped');
      }, 100)
    }
    socket.on('dumpbuffer', function (withOutChat) {
      let discussionId = mapSocketToDiscussion[socket.id]
      if (CONFIG.DEBUG) { console.log('dumpbuffer ' + discussionId) }
      redis.hget('workplace', discussionId, (err, workplace) => {
        if (err || workplace == null) {
          if (CONFIG.DEBUG) console.warn('WARNING: No work done on this workspace')
        }
        if (workplace) {
          workplace= JSON.parse(workplace);
          loadWorkspace(workplace, withOutChat)
        }
      })
    })
    socket.on('endsession', function(){
      let discussionId = mapSocketToDiscussion[socket.id]
      if (CONFIG.DEBUG) { console.log('endsession ' + discussionId) }
      var opts= {
        uri: CONFIG.API_URL+"/live_classes/close/"+discussionId,
        qs: {
            token: token
        },
        json: true
      }
      REQUEST(opts).then(()=>{}).catch(console.log);

      redis.hget('workplace', discussionId, (err, workplace) => {
        if (err && CONFIG.DEBUG) console.warn(err)
        if (workplace) {
          socket.to(discussionId).emit('endsession')
          workplace = JSON.parse(workplace)
          workplace['closed']=true;
          redis.hset('workplace', discussionId, JSON.stringify(workplace))
        }
      });
    });
    socket.on('clearworkspace', function(){
      let discussionId = mapSocketToDiscussion[socket.id]
      if (CONFIG.DEBUG) { console.log('clearworkspace ' + discussionId) }
      redis.hget('workplace', discussionId, (err, workplace) => {
        if (err && CONFIG.DEBUG) console.warn(err)
        if (workplace) {
          socket.to(discussionId).emit('clearworkspace')
          workplace = JSON.parse(workplace)
          workplace['tabs'] = []
          redis.hset('workplace', discussionId, JSON.stringify(workplace))
        }
      });
    });
    socket.on('tabadd', function (tabItem) {
      let discussionId = mapSocketToDiscussion[socket.id]
      if(discussionId != -1) {
        if (CONFIG.DEBUG) { console.log('tabadd ' + discussionId + ' ' + tabItem) }
        redis.hget('workplace', discussionId, (err, workplace) => {
          if (err && CONFIG.DEBUG) console.warn(err)
          if (workplace) {
            socket.to(discussionId).emit('tabadd', tabItem)
            tabItem = JSON.parse(tabItem)
            workplace = JSON.parse(workplace)
            if (!workplace['tabs']) workplace['tabs'] = []
            workplace['tabs'].push(tabItem)
            workplace['settings'][tabItem.id] = { 'ops': [] }
            redis.hset('workplace', discussionId, JSON.stringify(workplace));
          }
        })
      }
    })
    socket.on('tabrename', function (tabId,name) {
      let discussionId = mapSocketToDiscussion[socket.id]
      if (CONFIG.DEBUG) { console.log('tabrename ' + discussionId + ' ' + tabId + ' ' + name) }
      redis.hget('workplace', discussionId, (err, workplace) => {
        if (err && CONFIG.DEBUG) console.warn(err)
        if (workplace) {
          socket.to(discussionId).emit('tabrename', tabId, name)
          workplace = JSON.parse(workplace)
          if (!workplace['tabs']) workplace['tabs'] = []
          for (let i = 0; i < workplace['tabs'].length; i++) {
            if (workplace['tabs'][i]['id'] === tabId) {
              workplace['tabs'][i]['name']=name;
              break
            }
          }
          redis.hset('workplace', discussionId, JSON.stringify(workplace))
        }
      })
    })
    socket.on('tabremove', function (tabId) {
      let discussionId = mapSocketToDiscussion[socket.id]
      if (CONFIG.DEBUG) { console.log('tabremove ' + discussionId + ' ' + tabId) }
      redis.hget('workplace', discussionId, (err, workplace) => {
        if (err && CONFIG.DEBUG) console.warn(err)
        
        if (workplace) {
          socket.to(discussionId).emit('tabremove', tabId)
          workplace = JSON.parse(workplace)
          if (!workplace['tabs']) workplace['tabs'] = []
          let tabIndex = -1
          for (let i = 0; i < workplace['tabs'].length; i++) {
            if (workplace['tabs'][i]['id'] === tabId) {
              tabIndex = i
              break
            }
          }
          if (tabIndex !== -1) workplace['tabs'].splice(tabIndex, 1)
          if(workplace['whiteboard']) delete workplace['whiteboard'][tabId]
          if(workplace['content']) delete workplace['content'][tabId]
          delete workplace['settings'][tabId]
          redis.hset('workplace', discussionId, JSON.stringify(workplace))
        }
      })
    })
    socket.on('tabchanged', function (tabId) {
      console.log('hello')
      let discussionId = mapSocketToDiscussion[socket.id]
      if (CONFIG.DEBUG) { console.log('tabchanged ' + tabId) }
      redis.hget('workplace', discussionId, (err, workplace) => {
        if (err && CONFIG.DEBUG) console.warn(err)
        
        if (workplace) {
          workplace = JSON.parse(workplace)
          workplace['currentTab']=tabId;
          redis.hset('workplace', discussionId, JSON.stringify(workplace))
        }
      })
      socket.to(discussionId).emit('tabchanged', tabId)
    })
    
    socket.on('raisehand', function () {
      let discussionId = mapSocketToDiscussion[socket.id]
      if (CONFIG.DEBUG) { console.log('raisedhand on ' + discussionId + ' by ' + socket.id) }
      redis.hget('presenters', discussionId, (err, presenter) => {
        if (err && CONFIG.DEBUG) console.warn(err)
        if (presenter) {
          presenter=JSON.parse(presenter);
          io.to(presenter.peerId).emit('raisehand', { user: user, timestamp: new Date() })
        }
      })
    });

    socket.on('layout',function (name){
      let discussionId = mapSocketToDiscussion[socket.id]
      if (CONFIG.DEBUG) { console.log('layout changed on ' + discussionId + ' by ' + socket.id) }
      redis.hget('workplace', discussionId, (err, workplace) => {
        if (err && CONFIG.DEBUG) console.warn(err)
        
        if (workplace) {
          workplace = JSON.parse(workplace)
          workplace['layout']=name;
          redis.hset('workplace', discussionId, JSON.stringify(workplace))
        }
      })
      socket.to(discussionId).emit('layout', name);
    });
    socket.on('updatepaused',function (tabId, mediaType, eventType, timestamp){
      let discussionId = mapSocketToDiscussion[socket.id]
      if (CONFIG.DEBUG) { console.log('updatepaused changed on ' + discussionId + ' by ' + socket.id) }
      socket.to(discussionId).emit('updatepaused', tabId, mediaType, eventType, timestamp);
    });
    socket.on('exporttemplate', function (templatename, type, cb) {
      let discussionId = mapSocketToDiscussion[socket.id]
      if (CONFIG.DEBUG) { console.log('exportingtemplate ' + discussionId) }
      redis.hget('workplace', discussionId, (err, workplace) => {
        if (err || workplace == null) {
          cb(true);
          if (CONFIG.DEBUG) console.warn('WARNING: No work done on this workspace')
        }else{
          console.log(workplace)
          workplace = JSON.parse(workplace)
          if(workplace['chat']) delete workplace['chat'];
          let fileContent=JSON.stringify(workplace);
          cb(null,fileContent);
          if(type=='save'){
            var opts= {
              method: 'POST',
              uri: CONFIG.API_URL+"/file_library",
              formData: {
                name: templatename,
                description: '',
                is_read: 0,
                is_write: 0,
                is_delete: 0,
                is_shared: 0,
                parent_id: 0,
                type: 1,
                content_type: 'template',
                filename: {
                  value: fileContent,
                  options: {
                    filename: 'template.json',
                    contentType: 'text/json'
                  }
                }
              },
              qs: {
                  token: token
              },
              json: true
            }
            return REQUEST(opts).then(function(resp){
              // ToDo: do something on success
            })
          }
        }
      });
    });
    socket.on('importfile', function (file,cb){
      let discussionId = mapSocketToDiscussion[socket.id]
        var opts= {
          method: 'POST',
          uri: CONFIG.API_URL+"/file_library",
          formData: {
            name: file.name.replace(/\.[^/.]+$/, ""),
            description: '',
            is_read: 0,
            is_write: 0,
            is_delete: 0,
            is_shared: 0,
            parent_id: 0,
            type: 1,
            filename: {
              value: Buffer.from(file.data,'binary'),
              options: {
                filename: file.name
              }
            }
          },
          qs: {
              token: token
          },
          json: true
        }
        return REQUEST(opts).then(function(resp){
          cb(null,resp);
        }).catch(function(err){
          cb(err.message,null);
        })
    });
    function importTemplate(discussionId, content, cb){
      try{
        let fileContent = JSON.parse(content);

        // If file doesn't contain any of those things then it must be a bad file 
        if (!fileContent['tabs'] && !workplace['whiteboard'] && !workplace['settings'] && !workplace['layout']){
          cb(true);
          return;
        }
        
        //loadWorkspace(fileContent);
        redis.hget('workplace', discussionId, (err, workplace) => {
          if (err && CONFIG.DEBUG) console.warn(err)
          if (workplace) {
            workplace = JSON.parse(workplace)
            if(workplace['chat']) fileContent['chat']=workplace['chat'];
            redis.hset('workplace', discussionId, JSON.stringify(fileContent));
            io.sockets.in(discussionId).emit('dumpbuffer');
          }
          cb(null);
        })
      }catch(ex){
        console.log(ex);
        cb(true);
        return;
      }
    }

    socket.on('importtemplatefile', function (content, cb) {
      let discussionId = mapSocketToDiscussion[socket.id]
      importTemplate(discussionId, content,cb);
    });
    socket.on('importtemplate', function (fileUrl, cb) {
      let discussionId = mapSocketToDiscussion[socket.id]
      REQUEST(fileUrl).then(function(data){
        importTemplate(discussionId, data,cb);
      }).catch(function(err){
        cb(true);
      })
    });
    // save the recording of classroom 
    socket.on('recordingData', function (data,fileName) {
        let discussionId = mapSocketToDiscussion[socket.id]
        var opts= {
          method: 'POST',
          uri: CONFIG.API_URL+"/file_library/recordings",
          formData: {
            discussionId: discussionId,
            name: fileName,
            description: '',
            is_read: 0,
            is_write: 0,
            is_delete: 0,
            is_shared: 0,
            parent_id: 0,
            type: 1,
            content_type: 'recording',
            filename: {
              value: data,
              options: {
                filename: fileName+'.webm'
              }
            }
          },
          qs: {
              token: token
          },
          json: true
        }
        return REQUEST(opts).then(function(resp){
          // ToDo: do something on success
        })
        // let pathExist=FS.existsSync(CONFIG.FILE_STORAGE+ loggedInUser.id);
        // FS.mkdir(CONFIG.FILE_STORAGE+loggedInUser.id,{recursive: true},function(err){
        //   if(err && !pathExist){ 
        //     console.log(err);
        //   }
        //   let file=fileName+'.webm';
        //   FS.appendFile(CONFIG.FILE_STORAGE+ loggedInUser.id +'/' + file, data, function (err) {
        //       if(err){ 
        //         console.log(err);
        //       }
        //       let images=JSON.stringify([file]);
        //       let url=file;
        //       CONN.query('SELECT  count(1) as file_exist from media_files where name=? ',[file], function (error, results, fields) {
        //         if(error){
        //           console.log(error); 
        //         }
        //         if(results[0].file_exist == 0){
        //           CONN.query('INSERT INTO media_files SET name=?,type=?,user_id=?,is_private=0, url =?, images=? ',[file, 'video', loggedInUser.id, url, images], function (error, results, fields) {
        //             if(error){
        //               console.log(error); 
        //             }
        //           })    
        //         }
        //       })
        //   }); 
        // })
    });
    socket.on('deletefolder', function(folderId){
      let childFolders=[];
      function getFolders(folderIds){
        // return new Promise(function(resolve, reject){
        //   CONN.query("SELECT id from media_categories WHERE parent_id IN (?) AND user_id=?",[folderIds, loggedInUser.id], function (error, results, fields) {
        //     if(error) return reject(error);
        //     if(results.length <= 0){
        //       resolve(); 
        //     }else{
        //       let childIds=[];
        //       results.forEach(function(folder){
        //         childIds.push(folder.id);
        //         childFolders.push(folder.id)
        //       });
        //       getFolders(childIds).then(function(){
        //         resolve();
        //       })
        //     }
        //   })
        // })
      }

      getFolders([folderId]).then(function(){
        childFolders.push(folderId);
        return Promise.all([
          // new Promise(function(resolve,reject){
          //   CONN.query("DELETE from media_categories WHERE id IN (?) AND user_id=?",[childFolders, loggedInUser.id], function (error, results, fields) {
          //     if(error){
          //       reject(error);
          //     }else{
          //       resolve();  
          //     }
          //   })
          // }),
          // new Promise(function(resolve,reject){
          //   CONN.query("DELETE from media_files WHERE media_category_id IN (?) AND user_id=?",[childFolders, loggedInUser.id], function (error, results, fields) {
          //     if(error){
          //       reject(error);
          //     }else{
          //       resolve();  
          //     }
          //   })
          // })
        ]);
      }).then(function(){
        socket.emit('folderdeleted');
      }).catch(function(err){
        console.log(err);
      })
    })
    socket.on('addfolder', function(folderName, isPrivate, folderId){
      // CONN.query('INSERT INTO media_categories SET name=?,parent_id=?,user_id=?,is_private=?',[folderName, folderId, loggedInUser.id, isPrivate], function (error, results, fields) {
        socket.emit('folderadded');
      // })
    })
    socket.on('loadmedia',function(dtOpts, isPrivate, folderId){
      let startRec=(dtOpts.page-1)*dtOpts.limit;
      let endRec=dtOpts.page*dtOpts.limit;
      if (CONFIG.DEBUG) { console.log('loadmedia by ' + socket.id) }
      var opts= {
        uri: CONFIG.API_URL+"/file_library/files?limit="+dtOpts.limit+"&search="+dtOpts.search+"&order="+dtOpts.order+"&dir="+dtOpts.dir+"&parent_id="+folderId+"&privacy=-1&file_type=mp3,ogg,acc,wav,mp4,webm,3gp,flv,avi,mov&offset="+startRec+"&page="+dtOpts.page,
        qs: {
            token: token
        },
        json: true
      }
      return REQUEST(opts).then(function(resp){
        socket.emit('loadmedia', JSON.stringify({
          page: dtOpts.page,
          total_pages: resp.last_page,
          mediaList: resp.data
        }));
      })
    });
    socket.on('loadembed',function(dtOpts){
      let startRec=(dtOpts.page-1)*dtOpts.limit;
      let endRec=dtOpts.page*dtOpts.limit;
      if (CONFIG.DEBUG) { console.log('loadembed by ' + socket.id) }
      var opts= {
        uri: CONFIG.API_URL+"/file_library/embeds?limit="+dtOpts.limit+"&search="+dtOpts.search+"&order="+dtOpts.order+"&dir="+dtOpts.dir+"&privacy=-1&offset="+startRec+"&page="+dtOpts.page,
        qs: {
            token: token
        },
        json: true
      }
      return REQUEST(opts).then(function(resp){
        socket.emit('loadembed', JSON.stringify({
          page: dtOpts.page,
          total_pages: resp.last_page,
          embedList: resp.data
        }));
      })
    });
    socket.on('loadtemplatefromlib',function(dtOpts, folderId){
      let startRec=(dtOpts.page-1)*dtOpts.limit;
      let endRec=dtOpts.page*dtOpts.limit;
      if (CONFIG.DEBUG) { console.log('loadtemplatefromlib by ' + socket.id) }
      var opts= {
        uri: CONFIG.API_URL+"/file_library/files?limit="+dtOpts.limit+"&search="+dtOpts.search+"&order="+dtOpts.order+"&dir="+dtOpts.dir+"&parent_id="+folderId+"&privacy=-1&file_type=templates&offset="+startRec+"&page="+dtOpts.page,
        qs: {
            token: token
        },
        json: true
      }
      return REQUEST(opts).then(function(resp){
        socket.emit('loadtemplatefromlib', JSON.stringify({
          page: dtOpts.page,
          total_pages: resp.last_page,
          templateList: resp.data
        }));
      })
    });
    
    socket.on('loadtemplate',function(dtOpts){
      let startRec=(dtOpts.page-1)*dtOpts.limit;
      let endRec=dtOpts.page*dtOpts.limit;
      if (CONFIG.DEBUG) { console.log('loadtemplate by ' + socket.id) }
      var opts= {
        uri: CONFIG.API_URL+"/file_library/templates?limit="+dtOpts.limit+"&search="+dtOpts.search+"&order="+dtOpts.order+"&dir="+dtOpts.dir+"&parent_id=0&privacy=-1&file_type=&offset="+startRec+"&page="+dtOpts.page,
        qs: {
            token: token
        },
        json: true
      }
      return REQUEST(opts).then(function(resp){
        socket.emit('loadtemplate', JSON.stringify({
          page: dtOpts.page,
          total_pages: resp.last_page,
          templateList: resp.data
        }));
      })
    });
    socket.on('loadcontent',function(dtOpts, isPrivate, folderId){
      let startRec=(dtOpts.page-1)*dtOpts.limit;
      let endRec=dtOpts.page*dtOpts.limit;
      if (CONFIG.DEBUG) { console.log('loadcontent by ' + socket.id) }
      var opts= {
        uri: CONFIG.API_URL+"/file_library/files?limit="+dtOpts.limit+"&search="+dtOpts.search+"&order="+dtOpts.order+"&dir="+dtOpts.dir+"&parent_id="+folderId+"&privacy=-1&file_type=jpg,jpeg,png,pdf,doc,docx,ppt,pptx,xls,xlsx&offset="+startRec+"&page="+dtOpts.page,
        qs: {
            token: token
        },
        json: true
      }
      return REQUEST(opts).then(function(resp){
        socket.emit('loadcontent', JSON.stringify({
          page: dtOpts.page,
          total_pages: resp.last_page,
          contentList: resp.data
        }));
      })
    });
  }
  return {
    init: initUtilities
  }
}
module.exports = Utility
