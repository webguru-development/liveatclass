"use strict"
const CONFIG=require('./config')
const MD5 = require('md5');
const REQUEST = require('request-promise')
module.exports={
	getUserInfoForDiscussion: (token,discussionId)=>{
		if( CONFIG.DEBUG ) { console.info( 'Checking user by token: ' + token + ' for discussion: '+ discussionId ) ; }
	  if(!token) throw new Error('No user token found');

    var opts= {
      method: 'POST',
      uri: CONFIG.API_URL+"/userProfile",
      qs: {
          token: token
      },
      json: true
    }
    return REQUEST(opts).then(function(resp){
      if(resp.code !='200'){
        throw new Error('Something is wrong');
      }
      console.log(resp);
      // if admin or superadmin mark them as presenter
      return {id: resp.user_data.user_id,identity: MD5(resp.user_data.user_id), presenter: resp.user_data.user_type_id <=3,firstName: resp.user_data.firstname,lastName: resp.user_data.lastname,profileImage: resp.user_data.profile_image}
    }).catch(function(err){
      throw new Error('User not logged in')
    })
	}
}