'use strict'
const CONFIG=require('./config');
var Whiteboard = function () {
  function initBoard (redis, socket, io, mapSocketToDiscussion, user) {
    function initWorkPlace(workplace, tabId, typeOfBoard, pageId){
      workplace = JSON.parse(workplace)
      if(typeOfBoard === 'content'){
        if (!workplace['content']) workplace['content'] = {}
        if (!workplace['content'][tabId]) workplace['content'][tabId] = {'currentPage': 1, 'pages': {}}
        if (pageId && !workplace['content'][tabId]['pages'][pageId]) workplace['content'][tabId]['pages'][pageId] = {'drawings': [],'currentPos': {}, 'zoom': {}, 'grid': {}}
      }else{
        if (!workplace['whiteboard']) workplace['whiteboard'] = {}
        if (!workplace['whiteboard'][tabId]) workplace['whiteboard'][tabId] = {'drawings': [],'currentPos': {},'zoom': {}, 'grid': {}}
      }
      return workplace;
    }
    socket.on('drawing', function (drawing,tabId, typeOfBoard, pageId) {
      let discussionId = mapSocketToDiscussion[socket.id]
      if (CONFIG.DEBUG) { console.log('drawing ' + discussionId + ' ' + drawing) }
      socket.to(discussionId).emit('drawing', drawing, tabId, typeOfBoard, pageId);
      redis.hget('workplace', discussionId, (err, workplace) => {
        if (err && CONFIG.DEBUG) console.warn(err)
        if (workplace) {
          workplace=initWorkPlace(workplace, tabId, typeOfBoard, pageId);
          if(typeOfBoard === 'content'){
            workplace['content'][tabId]['pages'][pageId]['drawings'].push(drawing)  
          }else{
            workplace['whiteboard'][tabId]['drawings'].push(drawing)
          }
          redis.hset('workplace', discussionId, JSON.stringify(workplace))
        }
      })
    });
    socket.on('drawingchange', function (drawing,tabId, typeOfBoard, pageId) {
      let discussionId = mapSocketToDiscussion[socket.id]
      if (CONFIG.DEBUG) { console.log('drawingchange ' + discussionId + ' ' + drawing) }
      socket.to(discussionId).emit('drawingchange', drawing, tabId, typeOfBoard, pageId);
      redis.hget('workplace', discussionId, (err, workplace) => {
        if (err && CONFIG.DEBUG) console.warn(err)
        if (workplace) {
          workplace=initWorkPlace(workplace, tabId, typeOfBoard, pageId);
          if(typeOfBoard === 'content'){
            for (var i = workplace['content'][tabId]['pages'][pageId]['drawings'].length - 1; i >= 0; i--) {
              if(workplace['content'][tabId]['pages'][pageId]['drawings'][i].id==drawing.id){
                workplace['content'][tabId]['pages'][pageId]['drawings'][i].data=drawing.data;
              }
            }
            
          }else{
            for (var i = workplace['whiteboard'][tabId]['drawings'].length - 1; i >= 0; i--) {
              if(workplace['whiteboard'][tabId]['drawings'][i].id==drawing.id){
                workplace['whiteboard'][tabId]['drawings'][i]=drawing;
              }
            }
          }
          redis.hset('workplace', discussionId, JSON.stringify(workplace))
        }
      })
    });
    socket.on('changepage', function (tabId,page) {
      setTimeout(() => {
        let discussionId = mapSocketToDiscussion[socket.id]
        if (CONFIG.DEBUG) { console.log('changepage ' + tabId + ' on '+ page) }
        redis.hget('workplace', discussionId, (err, workplace) => {
          if (err && CONFIG.DEBUG) console.warn(err)
          if (workplace) {
            workplace=initWorkPlace(workplace, tabId, 'content');
            workplace['content'][tabId]['currentPage'] = page
            redis.hset('workplace', discussionId, JSON.stringify(workplace))
          }
        })
        socket.to(discussionId).emit('changepage', tabId, page)
      }, 2000);
    })
    socket.on('move', function (event,tabId, typeOfBoard, pageId) {
      let discussionId = mapSocketToDiscussion[socket.id]
      if (CONFIG.DEBUG) { console.log('move ' + discussionId) }
      redis.hget('workplace', discussionId, (err, workplace) => {
        if (err && CONFIG.DEBUG) console.warn(err)
        
        if (workplace) {
          workplace=initWorkPlace(workplace, tabId, typeOfBoard, pageId);
          if(typeOfBoard==='content'){
            workplace[typeOfBoard][tabId]['pages'][pageId]['currentPos'] = event.position
          }else{
            workplace[typeOfBoard][tabId]['currentPos'] = event.position
          }
          redis.hset('workplace', discussionId, JSON.stringify(workplace))
        }
      })
      socket.to(discussionId).emit('move', event.position, tabId, typeOfBoard, pageId);
    });
    socket.on('zoom', function (event,tabId, typeOfBoard, pageId) {
      let discussionId = mapSocketToDiscussion[socket.id]
      if (CONFIG.DEBUG) { console.log('zoom ' + discussionId + ' ' + event) }
      redis.hget('workplace', discussionId, (err, workplace) => {
        if (err && CONFIG.DEBUG) console.warn(err)
        
        if (workplace) {
          workplace=initWorkPlace(workplace, tabId, typeOfBoard, pageId);
          if(typeOfBoard==='content'){
            workplace[typeOfBoard][tabId]['pages'][pageId]['zoom'] = event
          }else{
            workplace[typeOfBoard][tabId]['zoom'] = event
          }
          redis.hset('workplace', discussionId, JSON.stringify(workplace))
        }
      })
      socket.to(discussionId).emit('zoom', event, tabId, typeOfBoard, pageId);
    });
    socket.on('grid', function (event,tabId, typeOfBoard, pageId) {
      let discussionId = mapSocketToDiscussion[socket.id]
      if (CONFIG.DEBUG) { console.log('grid ' + discussionId + ' ' + event) }
      redis.hget('workplace', discussionId, (err, workplace) => {
        if (err && CONFIG.DEBUG) console.warn(err)
        
        if (workplace) {
          workplace=initWorkPlace(workplace, tabId, typeOfBoard, pageId);
          if(typeOfBoard==='content'){
            workplace[typeOfBoard][tabId]['pages'][pageId]['grid'] = event
          }else{
            workplace[typeOfBoard][tabId]['grid'] = event
          }
          redis.hset('workplace', discussionId, JSON.stringify(workplace))
        }
      })
      socket.to(discussionId).emit('grid', event, tabId, typeOfBoard, pageId);
    });
    socket.on('clear', function (tabId, typeOfBoard, pageId) {
      let discussionId = mapSocketToDiscussion[socket.id]
      if (CONFIG.DEBUG) { console.log('clear ' + discussionId) }
      socket.to(discussionId).emit('clear', tabId, typeOfBoard, pageId);
      redis.hget('workplace', discussionId, (err, workplace) => {
        if (err && CONFIG.DEBUG) console.warn(err)
        if (workplace) {
          workplace=initWorkPlace(workplace, tabId, typeOfBoard, pageId);
          if(typeOfBoard === 'content'){
            let tabIndex=workplace['tabs'].findIndex(tab=>{
              return tab.id==tabId;
            });
            workplace['tabs'][tabIndex]['content'][pageId]['drawings'] =[]  
            workplace['content'][tabId]['pages'][pageId]['drawings'] =[]  
          }else{
            workplace['whiteboard'][tabId]['drawings'] = []
          }
          redis.hset('workplace', discussionId, JSON.stringify(workplace))
        }
      })
    });
    socket.on('resetworkspace', function (tabId, typeOfBoard, pageId) {
      let discussionId = mapSocketToDiscussion[socket.id]
      if (CONFIG.DEBUG) { console.log('resetworkspace ' + discussionId) }
      socket.to(discussionId).emit('resetworkspace', tabId, typeOfBoard, pageId);
      redis.hget('workplace', discussionId, (err, workplace) => {
        if (err && CONFIG.DEBUG) console.warn(err)
        if (workplace) {
          workplace=initWorkPlace(workplace, tabId, typeOfBoard, pageId);
          if(typeOfBoard==='content'){
            workplace[typeOfBoard][tabId]['pages'][pageId]['zoom'] = {x:1,y:1}
            workplace[typeOfBoard][tabId]['pages'][pageId]['currentPos'] = {x:0,y:0}
          }else{
            workplace[typeOfBoard][tabId]['zoom'] = {x:1,y:1}
            workplace[typeOfBoard][tabId]['currentPos'] = {x:0,y:0}
          }
          redis.hset('workplace', discussionId, JSON.stringify(workplace))
        }
      })
    });
    socket.on('undo', function (tabId, typeOfBoard, pageId) {
      let discussionId = mapSocketToDiscussion[socket.id]
      if (CONFIG.DEBUG) { console.log('undo ' + discussionId) }
      socket.to(discussionId).emit('undo', tabId, typeOfBoard, pageId);
      redis.hget('workplace', discussionId, (err, workplace) => {
        if (err && CONFIG.DEBUG) console.warn(err)
        if (workplace) {
          workplace=initWorkPlace(workplace, tabId, typeOfBoard, pageId);
          if(typeOfBoard === 'content'){
            workplace['content'][tabId]['pages'][pageId]['drawings'].pop()  
          }else{
            workplace['whiteboard'][tabId]['drawings'].pop()
          }
          redis.hset('workplace', discussionId, JSON.stringify(workplace))
        }
      })
    });
    socket.on('removedrawing', function (id, tabId, typeOfBoard, pageId) {
      let discussionId = mapSocketToDiscussion[socket.id]
      if (CONFIG.DEBUG) { console.log('removedrawing ' + discussionId) }
      socket.to(discussionId).emit('removedrawing', tabId, typeOfBoard, pageId, id);
      redis.hget('workplace', discussionId, (err, workplace) => {
        if (err && CONFIG.DEBUG) console.warn(err)
        if (workplace) {
          workplace=initWorkPlace(workplace, tabId, typeOfBoard, pageId);
          if(typeOfBoard === 'content'){
            for (var i = workplace['content'][tabId]['pages'][pageId]['drawings'].length - 1; i >= 0; i--) {
              if(workplace['content'][tabId]['pages'][pageId]['drawings'][i].id==id){
                workplace['content'][tabId]['pages'][pageId]['drawings'].splice(i,1);
              }
            }
          }else{
            for (var i = workplace['whiteboard'][tabId]['drawings'].length - 1; i >= 0; i--) {
              if(workplace['whiteboard'][tabId]['drawings'][i].id==id){
                workplace['whiteboard'][tabId]['drawings'].splice(i,1);
              }
            }
          }
          redis.hset('workplace', discussionId, JSON.stringify(workplace))
        }
      })
    });
    
  }
  return {
    init: initBoard
  }
}
module.exports = Whiteboard
