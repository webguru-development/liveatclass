'use strict'

import Toasted from 'vue-toasted';
import VueYouTubeEmbed from 'vue-youtube-embed';
import vueVimeoPlayer from 'vue-vimeo-player';
import JsBandwidth from "./jsbandwidth";
import PdfMake from "pdfmake/build/pdfmake";
import PdfFonts from "pdfmake/build/vfs_fonts";
import VueKonva from 'vue-konva'
import Player from '@vimeo/player'
PdfMake.vfs = PdfFonts.pdfMake.vfs;
var Ace = require('./ace/ace');
const youtubeRegexp = /https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube(?:-nocookie)?\.com\S*[^\w\s-])([\w-]{11})(?=[^\w-]|$)(?![?=&+%\w.-]*(?:['"][^<>]*>|<\/a>))[?=&+%\w.-]*/ig;
(function () {
  var Vue = require('vue')
  Vue.use(Toasted)
  Vue.use(VueYouTubeEmbed)
  Vue.use(vueVimeoPlayer)
  Vue.use(VueKonva)
  Vue.filter('capitalize', function (value) {
    if (!value) return ''
    value = value.toString()
    return value.charAt(0).toUpperCase() + value.slice(1)
  })

  Vue.component('paginate', require('vuejs-paginate'))
  Vue.component('media-list', require('./components/media-list'))
  Vue.component('template-list', require('./components/template-list'))
  Vue.component('embed-list', require('./components/embed-list'))
  Vue.component('content-list', require('./components/content-list'))
  Vue.component('code-editor', require('./components/code-editor'))
  Vue.component('white-board', require('./components/white-board'))
  Vue.component('video-box',require('./components/video-box'))
  Vue.component('chat-box', require('./components/chat-box'))

  new Vue({
    el: '#discussion_board',
    data: {
      course: {},
      remainingDuration: {hours: 0,minutes: 0,seconds: 0},
      remainingMinutes: 0,
      mainRoomRemainingMinutes : 0,
      isVidAvailable: false,
      isAudioAvailable: false,
      isRecordingAvailable: false,
      isScreenShared: false,
      isExpanded: true,
      showMenu: false,
      showItem: { 'youtube': false, 'media': false, 'content': false },
      youtubeVideoId: '',
      layout: 'classroom-view',
      templateFile: null,
      contentTabs: [],
      socket: null,
      currentTab: null,
      isLoading: true,
      loggedInUser: null,
      discussionId: null,
      presenter: null,
      participants: {},
      allParticipants: {},
      breakoutName: '',
      selectedBreakouts: [],
      selectedParticipants: [],
      participantToRemove:{},
      participantList: [],
      breakouts: [],
      createRoomSuccess: '',
      isLeaderCheck: false,
      breakout: {name: '',participants: [],groupLeader: null,timeOut: 60},
      currentBreakout: null,
      breakoutIndex: -1,
      addBreakoutIndex: -1,
      remaining_participants: {},
      isBreakoutActive: false,
      totalParticipants: 0,
      showNotifications: false,
      showParticipants: false,
      showChatBox: true,
      showWebCamBox: false,
      removeWebCamBox: false,
      removeChatBox: false,
      notifications: [],
      showTimer: false,
      mediaList: [],
      mediaOpts: {
        limit: 10,
        search: '',
        page: 1,
        order: 'name',
        dir: 'asc',
      },
      contentOpts: {
        limit: 10,
        search: '',
        page: 1,
        order: 'name',
        dir: 'asc',
      },
      embedOpts: {
        limit: 10,
        search: '',
        page: 1,
        order: 'name',
        dir: 'asc',
      },
      templateOpts: {
        limit: 10,
        search: '',
        page: 1,
        order: 'name',
        dir: 'asc',
      },
      templateFileLibraryOpts: {
        limit: 10,
        search: '',
        page: 1,
        order: 'name',
        dir: 'asc',
      },
      templateList: [],
      templateFileLibraryList: [],
      embedList: [],
      speaker: {},
      isLoadingMedia: false,
      isLoadingEmbed: false,
      isLoadingTemplate: false,
      isLoadingTemplateFileLibrary: false,
      showContentUploadField: false,
      totalMediaPages: 0,
      totalEmbedPages: 0,
      currentMediaPage: 1,
      currentEmbedPage: 1,
      totalTemplatePages: 1,
      totalTemplateFileLibraryPages: 1,
      currentTemplateFileLibraryPage: 1,
      currentTemplatePage: 1,
      selectedEmbed: {},
      selectedContent: {},
      selectedMedia: {},
      selectedTemplate: {},
      selectedTemplateFile: {},
      templateName: '',
      contentList: [],
      folderList: [],
      currentFolder: {},
      showPrivate: null,
      isLoadingContent: false,
      totalContentPages: 0,
      currentContentPage: 1,
      volumePts: 0,
      selectedCamera: '',
      selectedMic: '',
      selectedSetting: 'video',
      videoDevices: [],
      audioDevices: [],
      connectivitySettings: {
          internet: navigator.onLine,
          browser: (typeof navigator.mediaDevices !='undefined'),
          connected: false,
          rtt: navigator.connection.rtt,
          downlink: navigator.connection.downlink,
          download: navigator.connection.downlink,
          upload: navigator.connection.downlink,
          latency: '0'
      },
      serverUrl: '',
      logoutTitle: null,
      netSpeedCheckLoaderShow: false,
      whiteboard_couter : 0,
      objhostVidT: null,
      maxSizeError : false
    },
    mounted: function () {
      var that = this
      that.$nextTick(function () {

        let objhostVid = new hostVideo();
        that.objhostVidT = objhostVid;


        // pick value from meta tags
        let token = document.querySelector('meta[key=token][value]').getAttribute('value')
        let discussionId = document.querySelector('meta[key=discussion_id][value]').getAttribute('value')
        //that.serverUrl = 'https://'+process.env.VD_HOST+':'+process.env.VD_PORT;
        that.serverUrl = document.querySelector('meta[key=socket_server_url][value]').getAttribute('value');

        // replace if found in query params
        let urlParams = new URLSearchParams(window.location.search)
        let paramToken = urlParams.get('token')
        let paramDiscussionId = urlParams.get('discussion_id')
        if (paramToken) token = paramToken
        if (paramDiscussionId) discussionId = paramDiscussionId

        that.discussionId = discussionId

        // objhostVid.hostOpenRoom(that.discussionId,'haidar') // host

        // objhostVid.joinRoom(that.discussionId) // join

        let raisedHands = {}
        function addParticipants (peers, typeArray) {
          if (typeArray) {
            Object.keys(peers).forEach(function (peerId) {
              peers[peerId].peerId = peerId
              that.participants[peers[peerId].identity] = peers[peerId]
              if (peers[peerId].presenter) that.presenter = peers[peerId]
            })
          } else {
            that.participants[peers.identity] = peers
          }
        }
        window.addEventListener('resize', that.handleResize)
        window.addEventListener('click', that.handleClick)
        //document.querySelector('.no-video-img').addEventListener('load', that.handleResize)
        that.handleResize()

        let bufferDumped = false
        let serverConnectError
        // make socket connection
        let io = require('socket.io-client')(that.serverUrl, {transports: ['websocket'], query: 'token=' + token + '&discussion_id=' + discussionId })
        io.on('disconnect', () => {
          serverConnectError = Vue.toasted.error('Server is offline!', { position: 'bottom-right' })
          that.isLoading = true
          that.connectivitySettings.connected=false;
        })
        io.on('connect', () => {
          console.log('Socket connected');
          if (serverConnectError) serverConnectError.goAway(0)
          that.connectivitySettings.connected=true;
          that.socket = io
          that.socket.on('discussiondetail', function (course) {
            console.log(course);
            that.course = course;
            that.showChatBox=course.private_chat==1;
            that.checkToggle();
            let intervalTimer=setInterval(_=>{
             let secondRemain=that.calculateRemainingTime();
             if(secondRemain <= 0 && course.live){
              clearInterval(intervalTimer);
              that.endSession(true);
             }
            },1000)
          })
          that.socket.on('validuser', function (user) {
            // console.log(user);
            if(user.presenter){
              objhostVid.hostOpenRoom(that.discussionId,user.firstName)
            }else{
              let isJoin = objhostVid.joinRoom(that.discussionId,user.firstName);
              if(isJoin){
                getStremId();
              }else{
                setTimeout(() => {
                  getStremId();
                }, 5000);
              }

              function getStremId(){
                let streamId= objhostVid.getMyVideoSteamId();
                console.log(streamId);
              }
            }

            that.loggedInUser = user
            that.isLoading = false
            that.checkToggle();
            that.socket.emit('dumpbuffer');
            that.logoutTitle = that.loggedInUser.presenter ? 'Logout' : 'Leave the class';
          })

          that.socket.on('changeleader', function (identity) {
            that.currentBreakout.groupLeader= identity;
          })
          that.socket.on('invaliduser', function (msg) {
            Vue.toasted.error(msg, { position: 'bottom-right' }).goAway(2000)
            setTimeout(() => {
              Vue.toasted.error('Redirecting to home..', { position: 'bottom-right' })
              setTimeout(()=>{window.location.href='/'},1000)
            }, 3000)
          })
          that.socket.on('tabadd', function (tabItem) {
            that.addContainer(JSON.parse(tabItem))
          })
          that.socket.on('tabremove', function (tabId) {
            that.removeContainer(tabId, true)
          })
          that.socket.on('layout', function (name){
            that.layout=name;
          });
          that.socket.on('tabrename', function (tabId,name) {
            for (let i = 0; i < that.contentTabs.length; i++) {
              if (that.contentTabs[i].id === tabId) {
                that.contentTabs[i]['name']=name;
                break
              }
            }
          })
          that.socket.on('tabchanged', function (tabId) {
            that.currentTab = tabId
            that.handleResize();
          })
          that.socket.on('raisehand', function (info) {
            if (raisedHands.hasOwnProperty(info.user.identity)) return
            raisedHands[info.user.identity] = 1
            let msg='raised a request!';
            that.notifications.push({sender:  info.user.firstName, text: msg,time: new Date()})
            Vue.toasted.info(info.user.firstName +" "+msg, { position: 'bottom-right',
              action: [
                /*{
                  text: 'Chat',
                  onClick: (e, toast) => {
                    delete raisedHands[info.user.identity]
                    that.$refs.chatBox.chatWith(info.user.identity)
                    toast.goAway(0)
                  }
                },*/
                {
                  text: 'Close',
                  onClick: (e, toast) => {
                    delete raisedHands[info.user.identity]
                    toast.goAway(0)
                  }
                }
              ] })
          })
          that.socket.on('participants', function (peers) {
            addParticipants(peers, true)
            that.totalParticipants = Object.keys(that.participants).length
          })
          that.socket.on('allparticipants',function (allParticipants){
            Object.keys(allParticipants).forEach(function (peerId) {
              allParticipants[peerId].peerId = peerId
              that.allParticipants[allParticipants[peerId].identity] = allParticipants[peerId]
            })
          })
          that.socket.on('breakouts', function (breakouts) {
            that.breakouts = breakouts;
          })
          that.socket.on('peer-connect', function (data) {
            data.user.peerId = data.socketId
            that.notifications.push({sender: data.user.firstName, text: 'is online!',time: new Date()});
            addParticipants(data.user, false)
            that.allParticipants[data.user.identity]=data.user;
            that.totalParticipants = Object.keys(that.participants).length
            if (data.user.presenter === true) {
              Vue.toasted.success('Presenter is online!', { position: 'bottom-right' }).goAway(1000)
              that.presenter = data.user
            }
            that.manageBreakout();
          })
          that.socket.on('peer-connect-global', function (data) {
            if(that.loggedInUser.presenter){
              that.allParticipants[data.user.identity]=data.user;
            }
          })
          that.socket.on('disconnect', function () {
            that.participants = {}
            that.allParticipants={};
            that.totalParticipants = 0
          })
          that.socket.on('changepermission', function(action, value){
            that.loggedInUser.permissions[action]=value;
            if(action == 'video') {
              that.toggleVideo();
            }
            if(action == 'audio') {
              that.toggleAudio();
            }
          })
          that.socket.on('peer-disconnect-global', function (data) {
            let peerIdentity
            Object.keys(that.allParticipants).forEach(identity => {
              if (that.allParticipants[identity].peerId === data.socketId) {
                peerIdentity = identity
              }
            })
            if (peerIdentity) {
              delete that.allParticipants[peerIdentity];
            }
          });
          that.socket.on('peer-disconnect', function (data) {
            let peerIdentity
            Object.keys(that.allParticipants).forEach(identity => {
              if (that.allParticipants[identity].peerId === data.socketId) {
                peerIdentity = identity
              }
            })
            if (peerIdentity) {
              that.notifications.push({sender: that.participants[peerIdentity].firstName, text: 'is offline!',time: new Date()});
              delete that.participants[peerIdentity]
              delete that.allParticipants[peerIdentity];
            }

            that.participants=Object.assign({},that.participants);
            that.remaining_participants=Object.assign({},that.participants);
            if(that.breakout.participants.length){
              that.breakout.participants.forEach(function(identity){
                if(that.remaining_participants[identity]){
                  delete that.remaining_participants[identity];
                }
              })
              let partIn=that.breakout.participants.indexOf(peerIdentity);
              if(partIn !== -1){
                that.breakout.participants.splice(peerIdentity,1);
              }
            }

            that.totalParticipants = Object.keys(that.participants).length
            if (that.presenter === null) return
            if (data.socketId === that.presenter.peerId) {
              Vue.toasted.error('Presenter is gone offline!', { position: 'bottom-right' }).goAway(1000)
              that.presenter = null
            }
          })
          that.socket.on('loadtemplatefromlib', function (templateData) {
            templateData = JSON.parse(templateData)
            that.isLoadingTemplateFileLibrary=false
            that.templateFileLibraryList = templateData.templateList
            that.currentTemplateFileLibraryPage = templateData.page
            that.totalTemplateFileLibraryPages = templateData.total_pages
          })
          that.socket.on('loadtemplate', function (templateData) {
            templateData = JSON.parse(templateData)
            that.isLoadingTemplate=false
            that.templateList = templateData.templateList
            that.currentTemplatePage = templateData.page
            that.totalTemplatePages = templateData.total_pages
          })
          that.socket.on('loadmedia', function (mediaData) {
            mediaData = JSON.parse(mediaData)
            that.mediaList = mediaData.mediaList
            that.currentMediaPage = mediaData.page
            that.totalMediaPages = mediaData.total_pages
            that.isLoadingMedia = false
          })
          that.socket.on('loadembed', function (embedData) {
            embedData = JSON.parse(embedData)
            that.embedList = embedData.embedList
            that.currentEmbedPage = embedData.page
            that.totalEmbedPages = embedData.total_pages
            that.isLoadingEmbed = false
          })
          that.socket.on('loadcontent', function (contentData) {
            contentData = JSON.parse(contentData)
            that.contentList = contentData.contentList
            that.currentContentPage = contentData.page
            that.totalContentPages = contentData.total_pages
            that.isLoadingContent = false
          })
          that.socket.on('folderadded', function(){
            that.changeContentPage(1);
          });
          that.socket.on('folderdeleted', function(){
            Vue.toasted.success('Folder deleted!', { position: 'bottom-right' }).goAway(1000)
            that.changeContentPage(1);
          })
          that.socket.on('fileadded', function () {
            Vue.toasted.success('File added successfully!', { position: 'bottom-right' }).goAway(1000)
            that.changeContentPage(1);
          });
          that.socket.on('fileaddfailed', function () {
            Vue.toasted.error('File addition failed!', { position: 'bottom-right' }).goAway(1000)
          });
          that.socket.on('reconnect', function(){
            window.location.reload()
          })
          // that.socket.on('isBreakoutActive', function(isActive){
          //   that.isBreakoutActive=isActive;
          // })
          that.socket.on('endsession', function(){
            that.endSession();
          });
          that.socket.on('breakoutinfo', function(info){
            that.currentBreakout=info.breakout;
            that.course.startTime=info.breakout.startTime;
            that.course.duration=info.breakout.timeOut;
            that.breakoutIndex=info.breakoutIndex;
          })
          that.socket.on('clearworkspace', function(){
            that.contentTabs = []
            that.currentTab=null
          })
          that.socket.on('dumpbuffer', function () {
            that.contentTabs = []
            that.currentTab=null
            that.socket.emit('dumpbuffer', true);
          });

          //add presenter and particepants start
          /*console.log(that.allParticipants.presenter);
          Object.entries(that.allParticipants).forEach(function (Participant){
              console.log(Participant);
          });*/


          that.socket.on('updatepaused', function (tabId, mediaType, type, timestamp){
            let elem,playerId;
            switch(mediaType){
              case 'youtube-player':
                if(typeof YT == "undefined") return;
                playerId=document.getElementById('tab_'+tabId).querySelector('iframe').id;
                elem=YT.get(playerId);
                if(!elem) return;

                elem.seekTo(timestamp);
                switch(type){
                  case 'play':
                    elem.playVideo();
                  break;
                  case 'pause':
                    elem.pauseVideo();
                  break;
                }
              break;
              case 'vimeo-player':
                if(typeof Player == "undefined") return;
                playerId=document.getElementById('tab_'+tabId).querySelector('iframe');
                elem=new Player(playerId);
                if(!elem) return;

                elem.setCurrentTime(timestamp).then(function(){
                  switch(type){
                    case 'play':
                      elem.play();
                    break;
                    case 'pause':
                      elem.pause();
                    break;
                  }
                });
              break;
              default:
                elem=document.getElementById(mediaType+"_"+tabId);
                if(!elem) return;

                elem.currentTime=timestamp;
                switch(type){
                  case 'play':
                    elem.play();
                  break;
                  case 'pause':
                    elem.pause();
                  break;
                }
            }
          })
          // if (!bufferDumped) {
          //   setTimeout(function(){
          //     that.socket.emit('dumpbuffer')
          //   },1500);
          //   bufferDumped = true
          // }
        })
        that.getSpeedInfo();
      });
    },
    methods: {
      changeOpts: function(opts, type){
        switch(type){
          case 'content':
            this.contentOpts=opts;
            this.changeContentPage(1);
          break;
          case 'media':
            this.mediaOpts=opts;
            this.changeMediaPage(1);
          break;
          case 'embed':
            this.embedOpts=opts;
            this.changeEmbedPage(1);
          break;
          case 'import':
            this.templateOpts=opts;
            this.changeTemplatePage(1);
          break;
          case 'import_from_library':
            this.templateFileLibraryOpts=opts;
            this.changeTemplateFileLibraryPage(1);
          break;
        }
      },
      changeEmbedPage: function (pageNum) {
        this.isLoadingEmbed = true
        this.currentEmbedPage = pageNum
        this.embedOpts.page = pageNum
        this.socket.emit('loadembed', this.embedOpts)
      },
      changeMediaPage: function (pageNum) {
        this.isLoadingMedia = true
        this.currentMediaPage = pageNum
        this.mediaOpts.page=pageNum
        this.socket.emit('loadmedia', this.mediaOpts, this.showPrivate, this.currentFolder.id ? this.currentFolder.id : 0)
      },
      changeTemplatePage: function (pageNum) {
        this.isLoadingTemplate=true;
        this.currentTemplatePage = pageNum
        this.templateOpts.page=pageNum
        this.socket.emit('loadtemplate', this.templateOpts)
      },
      changeTemplateFileLibraryPage: function (pageNum) {
        this.isLoadingTemplateFileLibrary=true;
        this.currentTemplateFileLibraryPage = pageNum
        this.templateFileLibraryOpts.page=pageNum
        this.socket.emit('loadtemplatefromlib', this.templateFileLibraryOpts, this.currentFolder.id ? this.currentFolder.id : 0)
      },
      clearWorkspace: function(){
        this.contentTabs=[];
        this.currentTab=null;
        this.socket.emit('clearworkspace');
      },
      clearNotifications: function(){
        this.notifications=[];
      },
      changeContentPage: function (pageNum) {
        this.isLoadingContent = true
        this.currentContentPage = pageNum
        this.contentOpts.page=pageNum
        this.socket.emit('loadcontent', this.contentOpts, this.showPrivate, this.currentFolder.id ? this.currentFolder.id : 0)
      },
      goBack: function (){
        if(typeof this.currentFolder.id=='undefined'){
          this.showPrivate=null;
          this.contentList=[];
        }else if(this.currentFolder.parent_id ==0){
          this.currentFolder={}
          this.privateContent(this.showPrivate);
        }else{
          this.currentFolder={id: this.currentFolder.parent_id}
          this.changeContentPage(1);
        }
      },
      changeLayout: function (e){
        this.layout=e.target.value;
        document.querySelector('.webcam-container').style="";
        document.querySelector('.chat-container').style="";
        this.socket.emit('layout', e.target.value);
      },
      chatWith: function (identity) {
        if (identity !== this.loggedInUser.identity) {
          this.$refs.chatBox.chatWith(identity)
        }
      },
      languageChanged: function (lang, tabId) {
        let that=this;
        for (let i = 0; i < that.contentTabs.length; i++) {
          if (that.contentTabs[i].id === tabId) {
            that.contentTabs[i].language = lang
            break
          }
        }
        that.socket.emit('editorchange_language', lang, tabId)
      },
      handleClick: function (ev) {
        /*if (!document.querySelector('.timer-container').contains(ev.target)) {
          this.showTimer=false
        }*/
        if (!document.querySelector('.notification-container').contains(ev.target)) {
          this.showNotifications = false
        }
        if (!document.querySelector('.participant-container').contains(ev.target)) {
          this.showParticipants = false
        }
        if (!document.querySelector('#play-menu .dropdown').contains(ev.target)) {
          this.toggleMenu(true)
        }
      },
      endSession: function(emit){
        this.isLoading=true;
        Vue.toasted.info('Discussion has been ended. You will be redirected to home page soon...', { position: 'bottom-right' })
        if(emit) this.socket.emit('endsession');
        setTimeout(()=>{window.location.href='/'},3000)
      },
      calculateRemainingTime: function(){
        let moment=require('moment')
        let currentTime=moment.utc();
        let courseStartTime=moment.utc(this.course.startTime);
        let duration=moment.duration(courseStartTime.add(this.course.duration,'minutes').diff(currentTime));
        // if(currentTime >= courseStartTime){
        //   duration=moment.duration(courseStartTime.add(this.course.duration,'minutes').diff(currentTime));
        // }
        if(duration.asSeconds() <= 0) return 0;
        this.remainingDuration.hours=Math.floor(duration.asHours());
        this.remainingDuration.minutes=duration.minutes();
        this.remainingDuration.seconds=duration.seconds();
        this.remainingMinutes=duration.asMinutes();
        return duration.asSeconds();
      },
      drawRemainingTime: function () {
        let moment=require('moment')
        let remainingTimePercent=100;
        let currentTime=moment();
        let courseStartTime=moment.utc(this.course.startTime);
        if(currentTime >= courseStartTime){
          let duration=moment.duration(courseStartTime.add(this.course.duration,'minutes').diff(currentTime));
          let remainingDuration=duration.asMinutes();
          if(remainingDuration > 0){
            remainingTimePercent=(remainingDuration/this.course.duration) * 100
          }else{
            remainingTimePercent=0;
          }
        }
        var a = (remainingTimePercent * 360) / 100
        a %= 360
        var r = Math.PI; var r = (a * r / 180)
        var x = Math.sin(r) * 125
        var y = Math.cos(r) * -125
        var mid = (a > 180) ? 1 : 0
        var anim = 'M 0 0 v -125 A 125 125 1 ' +
                 mid + ' 1 ' +
                 x + ' ' +
                 y + ' z'
        return anim
      },
      showParticipantList: function (index){
        this.participantList.push(index);
        console.log(this.participantList);
      },
      hideParticipantList: function (index){
        let currentIndex=this.participantList.indexOf(index);
        if(currentIndex != -1){
          this.participantList.splice(currentIndex,1);
        }else{
          this.participantList.push(index);
        }
        console.log(this.participantList);
      },
      //changeGroupLeader: function (index, identity, peerId) {
      changeGroupLeader: function (index, identity) {
        console.log(this.showItem);
        //this.socket.emit('changeleader', index, identity);
      },
      addParticipants: function (index) {
        let that=this;
        that.selectedParticipants.forEach(function(participant,participantIndex){
          if(participantIndex==0 && that.breakouts[index].participants.length ==0) that.breakouts[index].groupLeader=participant;
          that.breakouts[index].participants.push(participant);
          that.breakouts[index].isgroupleader = that.isLeaderCheck;
          delete that.remaining_participants[participant];
          that.socket.emit('addparticipant',index, participant, that.allParticipants[participant].peerId, that.isLeaderCheck);
        });
        that.manageBreakout();
        that.toggleModel('participants');
        that.toggleModel('breakout');
      },
      addParticipantToBreakout: function (index) {
        let that=this;
        that.selectedParticipants=[];
        that.addBreakoutIndex=index;
        that.breakout=Object.assign({},that.breakouts[index]);
        that.toggleModel('participants');
        that.toggleModel('breakout');
      },
      confirmParticipantRemove: function (index, identity) {
        let that=this;
        that.participantToRemove={'index': index,'identity': identity};
        that.toggleModel('confirmbox-participant');
      },
      removeParticipant: function (index, identity) {
        let that=this;
        let breakout=that.breakouts[index];
        let userIndex=breakout.participants.indexOf(identity);
        breakout.participants.splice(userIndex,1);
        if(breakout.groupLeader == identity){
          breakout.groupLeader=null;
          if(breakout.participants.length) breakout.groupLeader=breakout.participants[0];
        }
        that.remaining_participants[identity]=Object.assign({},that.participants[identity]);
        that.breakouts[index]=breakout;
        that.manageBreakout();
        that.socket.emit('delparticipant',index, identity, that.allParticipants[identity].peerId);
      },
      createBreakout: function (){
        let that = this;
        console.log(that.remainingMinutes);

        let maxMinutes=that.remainingMinutes;
        if(maxMinutes > 60){
          maxMinutes=60;
        }
        that.breakouts.push({name: that.breakoutName,participants: [],groupLeader: null,isgroupleader: false,timeOut: maxMinutes,startTime: new Date().getTime()});
        that.socket.emit('createbreakout',that.breakouts);
        that.manageBreakout();
        that.createRoomSuccess = 'Breakout room created successfully, go to manage breakout to add participants and configure the room!';

        //that.toggleModel('createbreakout');
        //that.toggleModel('createbreakoutmessage');
      },
      // createBreakout: function () {
      //   let that =this;
      //   //that.isBreakoutActive=true;
      //   that.breakouts.push(that.breakout);
      //   that.breakout={participants: [],groupLeader: null,timeOut: 60,startTime: new Date().getTime()};
      //   that.socket.emit('createbreakout',that.breakouts);
      // },
      changeRoom: function (identity, room) {
        let that=this;
        if(room==0){
          let userIndex=that.breakout.participants.indexOf(identity);
          that.breakout.participants.splice(userIndex,1);
          if(that.breakout.groupLeader == identity){
            that.breakout.groupLeader=null;
            if(that.breakout.participants.length) that.breakout.groupLeader=that.breakout.participants[0];
          }
          that.remaining_participants[identity]=Object.assign({},that.participants[identity]);
        }else{
          if(that.breakout.participants.length < 1){
            that.breakout.groupLeader=identity;
            that.breakout.timeOut=60;
          }
          that.breakout.participants.push(iidentitydentity);
          if(that.remaining_participants[identity]) delete that.remaining_participants[identity];
        }
      },
      enterRoom: function (event) {
        //console.log(this.breakouts);

        let value=event.target.value;
        /*console.log(value);
        return;*/
        //event.target.value='-1';
        if(value=='create'){
          this.toggleModel('createbreakout');
          return;
        }
        if(value=='manage'){
          this.toggleModel('breakout');
          return;
        }
        this.socket.emit('enterroom',value);
      },
      changeTimeout: function (index,breakout){
        let that=this;
        if(breakout.timeOut >= that.remainingMinutes){
          this.toggleModel('breakout');
          this.toggleModel('breakout-timeout-limit');
        }else{
          that.socket.emit('createbreakout',that.breakouts);
        }
      },
      endBreakout: function () {
        let that=this;
        this.selectedBreakouts.forEach(function(index){
          that.socket.emit('endbreakout',index);
        })
        setTimeout(function(){
          window.location.reload();
        },200);
      },
      endAllBreakout: function () {
        let that=this;
        this.breakouts.forEach(function(breakout,index){
          that.socket.emit('endbreakout',index);
        });
        setTimeout(function(){
          window.location.reload();
        },200);
      },
      exitBreakout: function(index){
        this.socket.emit('exitbreakout',index);
        setTimeout(function(){
          window.location.reload();
        },200);
      },
      manageBreakout: function () {
        let that=this
        that.breakout.participants=[];
        that.remaining_participants=Object.assign({},that.participants);
        that.breakouts.forEach(function(breakout){
          breakout.participants.forEach(function(identity){
            if(that.remaining_participants[identity]) delete that.remaining_participants[identity];
          });
        });
      },
      updatePausedYT: function (tabId, type, e){
        let player=e.target;
        this.socket.emit('updatepaused',tabId, 'youtube-player', type, player.getCurrentTime());
      },
      updatePausedVM: function (tabId, type, e, asda){
        if(e) this.socket.emit('updatepaused',tabId, 'vimeo-player', type, e.seconds);
      },
      updatePaused: function (tabId,e){
        this.socket.emit('updatepaused',tabId, e.target.localName, e.type, e.target.currentTime);
      },
      toggleMute: function (mediaId){
        let elem= document.getElementById(mediaId);
        if(!elem) return;
        if(typeof this.speaker[mediaId] == 'undefined') this.speaker[mediaId]=false;
        this.speaker[mediaId] = !this.speaker[mediaId];
        elem.muted=!this.speaker[mediaId];
      },
      checkToggle: function(){
        let that=this;
        if(that.course.id && that.loggedInUser){
          that.isVidAvailable=that.course.usermedia_on_entry==1 && that.loggedInUser.permissions['video'];
          that.isAudioAvailable=that.course.usermedia_on_entry==1 && that.loggedInUser.permissions['audio'];
          that.isRecordingAvailable=that.course.record_on_entry==1 && that.loggedInUser.presenter;
        }
      },
      toggleVideo: function () {
        let that=this;
        if(this.isVidAvailable){
          if(that.loggedInUser.presenter){
            that.objhostVidT.selfVideoMute('host');
          }else{
            that.objhostVidT.selfVideoMute('join');
          }
        }else{
          if(that.loggedInUser.presenter){
            that.objhostVidT.selfVideoUnmute('host');
          }else{
            that.objhostVidT.selfVideoUnmute('join');
          }
        }

        this.isVidAvailable = !this.isVidAvailable
        if(this.isVidAvailable && this.isScreenShared){
          this.isScreenShared=false;
        }
      },
      toggleAudio: function () {
        let that=this;
        //console.log(that.loggedInUser.presenter);
        if(this.isAudioAvailable){
          if(that.loggedInUser.presenter){
            that.objhostVidT.selfAudioMute('host');
          }else{
            that.objhostVidT.selfAudioMute('join');
          }
        }else{
          if(that.loggedInUser.presenter){
            that.objhostVidT.selfAudioUnmute('host');
          }else{
            that.objhostVidT.selfAudioUnmute('join');
          }
        }
        this.isAudioAvailable = !this.isAudioAvailable
      },
      toggleRecordingConfirmBox: function () {
        if(!this.isRecordingAvailable){
          this.toggleModel('confirmbox-rec');
          return;
        }
        this.toggleRecord();
      },

      toggleRecord: function(){
        this.isRecordingAvailable = !this.isRecordingAvailable
      },
      toggleShare: function(){
        this.isScreenShared = !this.isScreenShared
        if(this.isScreenShared){
          this.isVidAvailable=false;
        }
      },
      stopRecording: function () {
        this.isRecordingAvailable = false
      },
      stopScreenShare: function () {
        this.isScreenShared = false
      },
      toggleBox: function (box) {
        switch(box){
          case 'chat':
            this.showChatBox=!this.showChatBox;
          break;
          case 'webcam':
            this.showWebCamBox=!this.showWebCamBox;
          break;
        }
        this.handleResize();
      },
      toggleScreen: function () {
        if(!this.isScreenShared){
          this.objhostVidT.shareMyScreen();
          this.toggleModel('confirmbox-share');
          return;
        }
        this.toggleShare();
      },
      toggleMenu: function (hideMenu) {
        if (hideMenu) {
          this.showMenu = false
        } else {
          this.showMenu = !this.showMenu
        }
      },
      renameTab: function (index,doneRenaming) {
        this.contentTabs[index]['renaming']=!doneRenaming;
        if(doneRenaming){
          this.socket.emit('tabrename', this.contentTabs[index].id,this.contentTabs[index].name);
        }
      },
      doneRenaming: function (index,event) {
        if(event.key==='Enter')this.renameTab(index,true);
      },
      removeContainer: function (tabId, triggeredByEvent, e) {
        if (tabId === this.currentTab) this.currentTab = null
        for (let i = 0; i < this.contentTabs.length; i++) {
          if (this.contentTabs[i].id === tabId) {
            this.contentTabs.splice(i, 1)
            break
          }
        }
        this.isExpanded=true;
        this.handleResize();
        if (!triggeredByEvent) this.socket.emit('tabremove', tabId)
        if (e) e.stopPropagation()
      },
      handleResize: function (firstTime) {
        window.dispatchEvent(new Event('redraw'));
      },
      addItem: function (itemType) {
        let tabId = this.uuidv4()
        let tabItem = { id: tabId, type: itemType }
        switch (itemType) {
          case 'youtube':
            break
          case 'whiteboard':
            if(this.whiteboard_couter == 0){
              tabItem.name = 'White Board';
            }else{
              tabItem.name = 'White Board '+this.whiteboard_couter;
            }
            this.whiteboard_couter ++ ;
            this.addContainer(tabItem,true);
            this.socket.emit('tabadd', JSON.stringify(tabItem));
            this.socket.emit('tabchanged', tabId);
            break
          case 'media':
            break
          case 'content':
            break
          case 'code':
            tabItem.name = 'Code Editor'
            tabItem.language = 'javascript'
            this.addContainer(tabItem)
            this.socket.emit('tabadd', JSON.stringify(tabItem))
            this.socket.emit('tabchanged', tabId)
            break
        }
        this.toggleMenu()
      },
      importTemplate: function (template, model) {
        let that=this;
        that.toggleModel(model);
        let importingToast=Vue.toasted.info('Importing template <div class="d-flex justify-content-center loader-container"><div class="lds-ripple"><div class="border-dark"></div><div class="border-dark"></div></div></div>',{position: 'bottom-right'});
        that.contentTabs=[];
        that.socket.emit('importtemplate', template.filename, function (err) {
          importingToast.goAway(0);
          if(err){
            Vue.toasted.error('Import failed!',{position: 'bottom-right'}).goAway(1500);
            return;
          }
          Vue.toasted.success('Import complete',{position: 'bottom-right'}).goAway(1500);
        });

      },
      saveToPdf: function(){
        let that=this;
        Vue.toasted.info("Generating pdf from the content!",{position: 'bottom-right'}).goAway(3000);
        new Promise((resolve,reject)=>{
          let docDefinition={
            styles: {
              intialheader: {
                fontSize: 24,
                bold: true
              },
              header: {
                fontSize: 18,
                bold: true
              },
              subheader: {
                fontSize: 15,
                bold: true
              },
              quote: {
                italics: true
              },
              small: {
                fontSize: 8
              },
              code: {
                fontSize: 12,
                italics: true
              }
            }
          };
          let docIndex=[];
          let content=[
            {text: that.course.title, style: 'intialheader',alignment: 'center',margin: [0, 200]},
            {text: 'Table of Content', style: 'header',alignment: 'center',pageBreak: 'before'},
          ];
          let contentPages=[
          ];
          let mediaLinks=[];
          let isMediaPresent=false;
          that.contentTabs.forEach(function(tab){
            if(['whiteboard','content'].indexOf(tab.type) !==-1){
              docIndex.push({
                text: [
                        'Page #',
                        { pageReference: tab.id },
                        ': ',
                        { textReference: tab.id }
                      ]
              });
            }
            switch(tab.type){
              case 'whiteboard':
                contentPages.push({
                  text: tab.name,
                  style: 'header',
                  id: tab.id,
                  pageBreak: 'before'
                });
                contentPages.push({
                  image: document.getElementById(tab.id).querySelector('.konvajs-content').querySelector('canvas').toDataURL('image/png', 0.1),
                  width: 800,
                  height: 600,
                  margin: [0,10]
                });
              break;
              case 'content':
                contentPages.push({
                  text: tab.name,
                  style: 'header',
                  id: tab.id,
                  pageBreak: 'before'
                });
                contentPages.push({
                  image: document.getElementById(tab.id).querySelector('.konvajs-content').querySelector('canvas').toDataURL('image/png', 0.1),
                  width: 800,
                  height: 600,
                  margin: [0,10]
                });
              break;
            }
          });
          content.push({
            ul: docIndex
          });
          // combine intitial pages and content pages
          content=content.concat(contentPages);

          docDefinition.content = content;
          resolve(docDefinition);
        }).then(doc=>{
          let pdf = PdfMake.createPdf(doc);
          var saveData = (function () {
              var a = document.createElement("a");
              document.body.appendChild(a);
              a.style = "display: none";
              return function (blob, fileName) {
                var url;
                if(window.webkitURL){
                  url=window.webkitURL;
                }else if(window.URL && window.URL.createObjectURL){
                  url=window.URL;
                }else{
                  console.log("Browser doesn't support generating object url");
                  return false;
                }
                let href=url.createObjectURL(blob);
                a.href = href;
                a.download = fileName;
                a.click();
                url.revokeObjectURL(href);
              };
          }());
          return pdf.getBlob(function(blob){
            that.socket.emit('save-pdf', blob);
            saveData(blob,'Workspace.pdf');
          });
        }).catch(function(err){
          console.log(err);
          Vue.toasted.error(err,{position: 'bottom-right'}).goAway(3000);
        })
      },
      exportTemplate: function(templateName, type) {
        this.toggleMenu();
        this.toggleModel('template');
        let exportingToast=Vue.toasted.info((type=='save'? 'Saving' : 'Exporting')+' template <div class="d-flex justify-content-center loader-container"><div class="lds-ripple"><div class="border-dark"></div><div class="border-dark"></div></div></div>',{position: 'bottom-right'});
        this.socket.emit('exporttemplate', templateName, type, function (err,fileBuffer) {
          exportingToast.goAway(0);
          if(err){
            Vue.toasted.error((type=='save'? 'Save' : 'Export')+' failed!',{position: 'bottom-right'}).goAway(1500);
            return;
          }
          let actions=[];
          if(type=='export'){
            actions.push({
              text: 'Download',
              onClick: (e, toast) => {
                var element = document.createElement('a');
                element.setAttribute('href', 'data:text/json;charset=utf-8,' + encodeURIComponent(fileBuffer));
                element.setAttribute('download', 'template'+require('moment')().format('YMdHmms')+'.json');
                element.style.display = 'none';
                document.body.appendChild(element);
                element.click();
                document.body.removeChild(element);
                toast.goAway(0)
              }
            });
          }
          actions.push({
            text: 'Close',
            onClick: (e, toast) => {
              toast.goAway(0)
            }
          });
          Vue.toasted.success((type=='save'? 'Save' : 'Export')+' complete',{position: 'bottom-right',action: actions});
        });
      },
      readTemplate: function (e) {
        let file=e.target.files[0];
        let filePlaceholder='Choose file';
        this.templateFile=null;
        if(file){
          filePlaceholder=file.name;
          this.templateFile=file;
        }
        e.target.nextElementSibling.innerHTML=filePlaceholder;
      },
      loadEmbed: function(embed){
        let tabItem;
        let that=this;
        let loadingInfo=Vue.toasted.info('Loading embedded video...',{position: 'bottom-right'});
        return new Promise(function(resolve,reject){
          switch(embed.content_type){
            case 'youtube':
              that.getYoutubeIdFromURL(embed.filename).then(function(videoId){
                tabItem = { id: that.uuidv4(), name: 'Youtube - ' + videoId, type: 'youtube', videoId: videoId }
                resolve(tabItem);
              }).catch(reject);
            break;
            case 'vimeo':
              that.getVimeoIdFromURL(embed.filename).then(function(videoId){
                tabItem = { id: that.uuidv4(), name: 'Vimeo - ' + videoId, type: 'vimeo', videoId: videoId }
                resolve(tabItem);
              }).catch(reject);
            break;
            default:
              reject()
          }
        }).then(function(tab){
          that.addContainer(tabItem)
          that.socket.emit('tabadd', JSON.stringify(tabItem))
          loadingInfo.goAway(0);
        }).catch(err=>{
          loadingInfo.goAway(0);
          Vue.toasted.error('Video could not be loaded',{position: 'bottom-right'}).goAway(1500);
        })
      },
      getVimeoIdFromURL: function(url) {
        return fetch('https://vimeo.com/api/oembed.json?url='+url).then(function(resp){
            return resp.json();
        }).then(data=>{
          return data.video_id;
        });
      },
      getYoutubeIdFromURL: function(url) {
        return new Promise(function(resolve,reject){
          let id = url.replace(youtubeRegexp, '$1')

          if (id.includes(';')) {
            const pieces = id.split(';')

            if (pieces[1].includes('%')) {
              const uriComponent = decodeURIComponent(pieces[1])
              id = `http://youtube.com${uriComponent}`.replace(youtubeRegexp, '$1')
            } else {
              id = pieces[0]
            }
          } else if (id.includes('#')) {
            id = id.split('#')[0]
          }

          return resolve(id);
        })
      },
      loadVideo: function (videoId, type) {
        if (videoId.trim() === '') return
        let that=this;
        that.loadEmbed({content_type: type,filename: videoId}).then(function(){
          that.youtubeVideoId = ''
          that.toggleModel(type);
        });
      },
      loadMedia: function (media, dontTriggerModel) {
        let tabId = this.uuidv4()
        let tabItem = { id: tabId, name: 'Media - ' + media.name, type: 'media', url: media.filename, mediaType: (['mp3','wav','aac'].indexOf(media.type) !=-1 ? 'audio': 'video') }
        this.addContainer(tabItem)
        if(!dontTriggerModel) this.toggleModel('media')
        this.socket.emit('tabadd', JSON.stringify(tabItem))
      },
      changePermission: function (user, actionFor){
        let that=this;
        that.participants[user.identity].permissions[actionFor]=!that.participants[user.identity].permissions[actionFor]
        that.participants=Object.assign({},that.participants);
        that.socket.emit('changepermission', user.peerId, actionFor, that.participants[user.identity].permissions[actionFor]);
      },
      privateContent: function(isPrivate){
        this.showPrivate=isPrivate;
        this.changeContentPage(1);
      },
      importFile: function(model){
        let maxSize = 2;
        let that=this;
        that.maxSizeError = false;

        if(!that.templateFile) return;
        if(Math.ceil(that.templateFile.size/(1024*1024)) > maxSize){
          that.maxSizeError = true;
          return;
        }

        let importingToast=Vue.toasted.info('Importing to whiteboard <div class="d-flex justify-content-center loader-container"><div class="lds-ripple"><div class="border-dark"></div><div class="border-dark"></div></div></div>',{position: 'bottom-right'});

        let fileReader=new FileReader();
        fileReader.readAsBinaryString(that.templateFile);
        fileReader.onload= function(evt){
          that.socket.emit('importfile', {name: that.templateFile.name,type: that.templateFile.type, size: that.templateFile.size, data: fileReader.result}, function (err, resp) {
            importingToast.goAway(0);
            if(err || !resp.status){
              Vue.toasted.error('Import failed!',{position: 'bottom-right'}).goAway(1500);
              return;
            }
            Vue.toasted.success('Import complete',{position: 'bottom-right'}).goAway(1500);
            switch(model){
              case 'content_opts':
                that.loadContent(resp.data, true);
              break;
              case 'media_opts':
                that.loadMedia(resp.data, true);
              break;
            }
            that.templateFile=null;
            that.toggleModel(model);
            that.toggleMenu();
          });
        }
      },
      importTemplateFile: function(){
        let that=this;

        if(!that.templateFile) return;

        let importingToast=Vue.toasted.info('Importing template <div class="d-flex justify-content-center loader-container"><div class="lds-ripple"><div class="border-dark"></div><div class="border-dark"></div></div></div>',{position: 'bottom-right'});

        let fileReader=new FileReader();
        fileReader.readAsText(that.templateFile);
        fileReader.onload= function(evt){
          that.socket.emit('importtemplatefile', fileReader.result, function (err, resp) {
            importingToast.goAway(0);
            if(err || !resp.status){
              Vue.toasted.error('Import failed!',{position: 'bottom-right'}).goAway(1500);
              return;
            }
            Vue.toasted.success('Import complete',{position: 'bottom-right'}).goAway(1500);

            that.templateFile=null;
            that.toggleModel('template_opts');
            that.toggleMenu();
          });
        }
      },
      addFolder: function (folderName){
        this.socket.emit('addfolder', folderName, this.showPrivate, this.currentFolder.id ? this.currentFolder.id : 0);
      },
      deleteFolder: function (folder){
        this.socket.emit('deletefolder', folder.id);
      },
      selectEmbed: function (embed){
        this.selectedEmbed = embed;
      },
      selectMedia: function (media){
        this.selectedMedia=media;
      },
      selectTemplate: function (template){
        this.selectedTemplateFile=template;
      },
      selectContent: function (content){
        this.selectedContent=content;
      },
      loadFolder: function (folder,type){
        let that=this;
        that.currentFolder=folder;
        if(folder.id){
          let index=-1;
          that.folderList.forEach((file,fileIndex)=>{
            if(file.id== folder.id){
              index=fileIndex;
            }
          });
          if(index != -1){
            that.folderList.splice(index);
          }
          that.folderList.push(folder);
        }else{
         that.folderList.splice(0);
        }
        if(type=='content'){
          that.changeContentPage(1);
        }else if(type == 'template'){
          that.changeTemplateFileLibraryPage(1);
        }else{
          that.changeMediaPage(1);
        }

      },
      loadContent: function (content, dontTriggerModel) {
        let that=this;
        let tabId = that.uuidv4()
        let contentPages={};
        if(content.images) content.images.forEach(img=>{
          let pageId=that.uuidv4();
          let drawingId=that.uuidv4();
          let draggable=content.content_type=='pdf' ? 'false' : 'true';
          contentPages[pageId]={id: pageId, drawings: [{id: drawingId, type: 'Image',url: img, data: '{"attrs":{"x":5,"y":5,"resizable":'+draggable+', "draggable":'+draggable+',"id":"'+drawingId+'"},"className":"Image"}'}]}
        })
        let tabItem = { id: tabId, name: 'Content - ' + content.name, type: 'content', content: contentPages, contentType: content.content_type }
        that.addContainer(tabItem,true)
        if(!dontTriggerModel) that.toggleModel('content')
        that.socket.emit('tabadd', JSON.stringify(tabItem))
      },
      addContainer: function (tabItem,newItem) {
        tabItem.renaming=false;
        tabItem.new=!!newItem;
        if(tabItem.type==='content') tabItem.currentPage=1;
        this.contentTabs.push(tabItem)
        this.currentTab = tabItem.id
        this.isExpanded=(tabItem.type!=='content' && tabItem.type!=='whiteboard');
        this.handleResize()
      },
      setCurrentTab: function (tabId) {
        let that=this;
        for (let i = 0; i < that.contentTabs.length; i++) {
          if (tabId !== that.currentTab && that.contentTabs[i].id === that.currentTab) {
            that.renameTab(i,true);
          }
          if(tabId == that.contentTabs[i].id){
            this.isExpanded=(that.contentTabs[i].type!=='content' && that.contentTabs[i].type!=='whiteboard');
          }
        }
        that.currentTab = tabId

        that.socket.emit('tabchanged', tabId)
        that.handleResize();
      },
      toggleModel: function (itemType) {
        this.showItem[itemType] = !this.showItem[itemType]
        switch(itemType){
          case 'content':
            this.changeContentPage(1);
          break;
          case 'media':
            this.changeMediaPage(1);
          break;
          case 'embed':
            this.changeEmbedPage(1);
          break;
          case 'import':
            this.changeTemplatePage(1);
          break;
          case 'import_from_library':
            this.changeTemplateFileLibraryPage(1);
          break;
        }
        this.toggleMenu()
      },
      raiseHand: function () {
        this.socket.emit('raisehand')
        Vue.toasted.success('Request has been sent to presenter!', { position: 'bottom-right' }).goAway(1500)
      },
      resetRoom: function(){
        document.querySelector('.webcam-container').style="";
        document.querySelector('.chat-container').style="";
        this.removeWebCamBox=false;
        this.removeChatBox=false;
      },
      dragBox: function(className,e){
        if(e.clientY<= 5 || e.clientX <= 5) return;
        let rightWidth=250;
        document.querySelector('.'+className).style="z-index: 99999;position:fixed;top:"+(e.clientY - 10)+"px;left:"+(e.clientX - rightWidth+ 10)+"px;width: "+rightWidth+"px";
      },
      dragPopup: function(className, e, stopDragging){
        if(stopDragging){
          this.lastMouseCords=null;
          return;
        }
        if(!this.lastMouseCords){
          this.lastMouseCords=e;
        }
        if(e.clientY<= 5 || e.clientX <= 5) return;
        let leftChange=this.lastMouseCords.clientX - e.clientX;
        let topChange=this.lastMouseCords.clientY - e.clientY;
        let existingLeft=document.querySelector('.'+className).offsetLeft;
        let existingTop=document.querySelector('.'+className).offsetTop;
        document.querySelector('.'+className).style="margin:0px;z-index: 99999;left:"+(existingLeft - leftChange)+"px;top:"+(existingTop - topChange)+"px;";
        this.lastMouseCords=e;
      },
      isInFullScreen: function(){
        return (document.fullscreenElement && document.fullscreenElement !== null) ||
        (document.webkitFullscreenElement && document.webkitFullscreenElement !== null) ||
        (document.mozFullScreenElement && document.mozFullScreenElement !== null) ||
        (document.msFullscreenElement && document.msFullscreenElement !== null);
      },
      toggleFullscreen: function (elemId) {
        if (this.isInFullScreen()) {
          if (document.exitFullscreen) {
            document.exitFullscreen()
          } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen()
          } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen()
          } else if (document.msExitFullscreen) {
            document.msExitFullscreen()
          }
        } else {
          let docElm = document.getElementById(elemId)
          if (docElm.requestFullscreen) {
            docElm.requestFullscreen()
          } else if (docElm.mozRequestFullScreen) {
            docElm.mozRequestFullScreen()
          } else if (docElm.webkitRequestFullScreen) {
            docElm.webkitRequestFullScreen()
          } else if (docElm.msRequestFullscreen) {
            docElm.msRequestFullscreen()
          }
        }
      },
      uuidv4: function () {
        var cryptoObj = window.crypto || window.msCrypto
        return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
          (c ^ cryptoObj.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
        )
      },
      openSettings: function(){
        this.toggleModel('videosetting');
        this.changeDevice();
      },
      closeSettings: function(){
        if (window.testStream) {
          window.testStream.getTracks().forEach(function(track) {
            track.stop();
          });
        }
        this.toggleModel('videosetting');
      },
      changeDevice: function(){
        let that= this;
        /*if (window.testStream) {
          window.testStream.getTracks().forEach(function(track) {
            track.stop();
          });
        }
        if(window.stream){
         window.stream.getTracks().forEach(function(track) {
            track.stop();
          });
        }*/
        // this.isVidAvailable=false;
        // this.isAudioAvailable=false;


        const constraints = {
          audio: {
            deviceId: {ideal: this.selectedMic}
          },
          video: {
            deviceId: {ideal: this.selectedCamera}
          }
        };
        // const constraints = {
        //   audio: true,
        //   video: true
        // };
        navigator.mediaDevices.getUserMedia(constraints).
        then(function(stream){

          window.testStream=stream;
          console.log(window.testStream);
          document.getElementById('testVideo').srcObject=stream;

          // check audio
          let audioContext = new AudioContext();
          let analyser = audioContext.createAnalyser();
          //let microphone = audioContext.createMediaStreamSource(stream);
          let javascriptNode = audioContext.createScriptProcessor(2048, 1, 1);

          analyser.smoothingTimeConstant = 0.8;
          analyser.fftSize = 1024;

          //microphone.connect(analyser);
          analyser.connect(javascriptNode);
          javascriptNode.connect(audioContext.destination);
          javascriptNode.onaudioprocess = function() {
              var array = new Uint8Array(analyser.frequencyBinCount);
              analyser.getByteFrequencyData(array);
              var values = 0;

              var length = array.length;
              for (var i = 0; i < length; i++){
                values += (array[i]);
              }
              var average = values / length;
              that.volumePts = Math.round(average/10);
          }
        }).catch(function(error){
          console.log(error);
          var errorMsg = 'Either Webcam or microphone not available';
          Vue.toasted.error(errorMsg, { position: 'bottom-right' }).goAway(2500)
        });
      },
      updateConnectionStatus: function(){
        this.connectivitySettings.internet=navigator.onLine;
      },
      updateNetworkInfo: function(){
        this.connectivitySettings.downlink=navigator.connection.downlink;
        this.connectivitySettings.rtt=navigator.connection.rtt;
      },
      getSpeedInfo: function(){
         let that=this;
        that.netSpeedCheckLoaderShow = true;
        let jsBandwidth=new JsBandwidth();
        jsBandwidth.testSpeed({latencyTestUrl: that.serverUrl+'/check',downloadUrl: that.serverUrl+'/check',uploadUrl: that.serverUrl+'/check'}).then(function(result){
          that.connectivitySettings.download=(result.downloadSpeed < 0 || isNaN(result.downloadSpeed) ? result.downloadSpeed : Math.floor((result.downloadSpeed / 1000000) * 100) / 100);
          that.connectivitySettings.upload=(result.uploadSpeed < 0 || isNaN(result.uploadSpeed) ? result.uploadSpeed : Math.floor((result.uploadSpeed / 1000000) * 100) / 100);
          that.connectivitySettings.latency=result.latency;
          setTimeout(function() {
            that.netSpeedCheckLoaderShow = false;
          }, 1000);
        }, function(err) {
          that.connectivitySettings.download=0;
          that.connectivitySettings.upload=0;
          that.connectivitySettings.latency=0;
          setTimeout(function() {
            that.netSpeedCheckLoaderShow = false;
          }, 1000);
        });
      }
    },
    computed: {
      orderedNotifications: function(){
        return Object.assign([],this.notifications).reverse();
      }
    },
    watch: {
      course: function (course) {
        document.title = course.title
      },
      isVidAvailable: function(){
        this.showWebCamBox=this.isVidAvailable;
      }
    },
    created: function () {
      let that=this;
      that.course = { title: 'Devil-101 \uD83D\uDE08' }

      // get audio,video devices
      navigator.mediaDevices.enumerateDevices()
      .then(function(devices){
        devices.forEach(function(device){
          if (device.kind === 'audioinput') {
            that.audioDevices.push({
              id: device.deviceId,
              name: device.label || 'microphone ' + (that.audioDevices.length + 1)
            });
          }else if (device.kind === 'videoinput') {
            that.videoDevices.push({
              id: device.deviceId,
              name: device.label || 'camera ' + (that.videoDevices.length + 1)
            });
          }
        });
        if(that.audioDevices.length) that.selectedMic=that.audioDevices[0].id;
        if(that.videoDevices.length) that.selectedCamera=that.videoDevices[0].id;
      }).catch(function(error){
        Vue.toasted.error(error, { position: 'bottom-right' }).goAway(1500)
      });
      window.addEventListener('online', that.updateConnectionStatus);
      window.addEventListener('offline', that.updateConnectionStatus);

      let connection = navigator.connection || navigator.mozConnection || navigator.webkitConnection;
      connection.addEventListener('change', that.updateNetworkInfo);

    },
    beforeDestroy: function () {
      window.removeEventListener('resize', this.handleResize)
      window.removeEventListener('click', this.handleClick)
    }
  })
})()
