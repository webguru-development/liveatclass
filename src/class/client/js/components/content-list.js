module.exports={
    props: ['contentList','folderList','showPrivate', 'socket', 'currentFolder','selectedContent','contentOpts'],
    data: function (){
      return {
        folderName: '',
        sortBy: 'name',
        sortOrder: true
      }
    },
    template: '#content-list',
    methods: {
      loadContent: function (content) {
        this.$emit('load-content', content)
      },
      addFolder: function(){
        if(this.folderName.trim() !=''){
          this.$emit('add-folder', this.folderName);  
        }
      },
      sortTable: function (field){
        this.sortOrder=(this.sortOrder==field ? !this.sortOrder : true);
        this.sortBy=field;
        this.contentOpts.order=field;
        this.contentOpts.dir=(this.sortOrder ? 'asc' : 'desc');
        this.changeOpts();
      },
      uploadFile: function(e){
        let file = e.target.files[0];
        let that=this;
        that.socket.emit('add-file',(that.currentFolder.id ? that.currentFolder.id : 0),that.showPrivate, {name: file.name,type: file.type} , file);  
      },
      goBack: function (){
        this.$emit('go-back');  
      },
      deleteFolder: function (folder){
        this.$emit('delete-folder', folder)
      },
      changeOpts: function(){
        this.$emit('content-opts-change', this.contentOpts, 'content');
      },
      selectContent: function (content) {
        this.$emit('select-content', content)
      },
      changeFolder: function (folder) {
        this.$emit('load-folder', folder, 'content')
      },
      setPrivate: function () {
        this.$emit('private-content',true);
      },
      setPublic: function () {
        this.$emit('private-content',false);
      }
    }
  }