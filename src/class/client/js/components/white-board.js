module.exports={
    props: ['item','socket'],
    template: '#white-board',
    data: function () {
      return {
        paint: null
      }
    },
    methods: {
      goNext: function () {
        if(this.item.currentPage < Object.keys(this.item.content).length){
          this.item.currentPage +=1;
          this.switchPage();
        }
      },
      goPrev: function () {
        if(this.item.currentPage > 1){
          this.item.currentPage -=1;
          this.switchPage();
        }
      },
      getCurrentPage: function () {
        let that=this;
        that.item.currentPage=that.item.currentPage || 1
        return that.item.content[Object.keys(that.item.content)[that.item.currentPage - 1]];
      },
      switchPage: function (dontEmit) {
        let that=this;
        if(that.item.type==='content'){
          let currentPage=that.getCurrentPage();
          let image=currentPage.image;
          that.paint.changeTool('select');
          that.paint.clearWorkspace(true);
          
          var p = Promise.resolve();
          currentPage.drawings.forEach(drawing=>{
            p= p.then(()=>that.paint.addDrawing(drawing,true));
          });
          that.paint.goto(0,0,true);
          that.paint.redrawLocals();
          if(!dontEmit) that.socket.emit('changepage',that.item.id,that.item.currentPage);
        }
      },
      listenToSocket: function () {
        let that=this;
        that.socket.on('dumped', function(){
          let drawings;
          if(that.item.type==='content'){ 
            that.switchPage(true);
            return;
          }
          drawings=that.paint.localDrawings;
          var p = Promise.resolve();
          drawings.forEach(drawing=>{
            p = p.then(() => that.paint.addDrawing(drawing,true)); 
          });
        });
        that.socket.on('drawing',function (drawing, tabId, typeOfBoard, pageId, isDump) {
          if(typeof isDump == 'undefined') isDump=false;
          if (tabId !== that.item.id) return
          if(typeOfBoard === 'content'){
            that.item.content[pageId].drawings.push(drawing);
            if(!isDump){
              let currentPage=that.getCurrentPage();
              if(currentPage.id === pageId){
                that.paint.addDrawing(drawing,true);
              }
            }
          }else{
            if(!isDump){
              that.paint.addDrawing(drawing,true);
            }else{
              that.paint.localDrawings.push(drawing);
            }
          }
        });
        that.socket.on('drawingchange',function (drawing, tabId, typeOfBoard, pageId, isDump) {
          if(typeof isDump == 'undefined') isDump=false;
          if (tabId !== that.item.id) return
          if(typeOfBoard === 'content'){
            for (var i = that.item.content[pageId].drawings.length - 1; i >= 0; i--) {
              if(that.item.content[pageId].drawings[i].id==drawing.id){
                that.item.content[pageId].drawings[i]=drawing;
                break;    
              }
            }
            let currentPage=that.getCurrentPage();
            if(currentPage.id === pageId){
              that.paint.addDrawing(drawing,true);
            }
          }else{
            for (var i = that.paint.localDrawings.length - 1; i >= 0; i--) {
              if(that.paint.localDrawings[i].id==drawing.id){
                that.paint.localDrawings[i]=drawing;
                break;
              }
            }
            that.paint.addDrawing(drawing,true);
          }
        });
        that.socket.on('changepage',function (tabId,page,isDump) {
          if(typeof isDump == 'undefined') isDump=false;
          if (tabId !== that.item.id) return
          that.item.currentPage=page
          if(!isDump) that.switchPage(true);
        });
        that.socket.on('move',function (position,tabId) {
          if (!position.x || tabId !== that.item.id) return
          that.paint.goto(position, true);  
        });
        that.socket.on('zoom',function (event,tabId) {
          if (!event.zoomFactor || tabId !== that.item.id) return
          that.paint.setLocalZoom(event.zoom);
          that.paint.zoom(event.zoomFactor, true);  
        });
        that.socket.on('grid',function (event,tabId) {
          if (tabId !== that.item.id) return
          that.paint.showGrid(event.show, true);  
        });
        that.socket.on('undo',function (tabId, typeOfBoard, pageId) {
          if (tabId !== that.item.id) return
          if(typeOfBoard === 'content'){
            that.item.content[pageId].drawings.pop();
          }
          that.paint.undo(true);
        })
        that.socket.on('removedrawing',function (tabId, typeOfBoard, pageId, id) {
          if (tabId !== that.item.id) return
          if(typeOfBoard === 'content'){
            for (var i = that.item.content[pageId].drawings.length - 1; i >= 0; i--) {
              if(that.item.content[pageId].drawings[i].id==id){
                that.item.content[pageId].drawings.splice(i,1);
                break;    
              }
            }
          }
          that.paint.removeDrawing(id,true);
        })
        that.socket.on('clear',function (tabId, typeOfBoard, pageId) {
          if (tabId !== that.item.id) return
          if(typeOfBoard === 'content'){
            that.item.content[pageId].drawings =[];
          }
          that.paint.clearWorkspace(true);
        })
        that.socket.on('resetworkspace',function (tabId, typeOfBoard, pageId) {
          if (tabId !== that.item.id) return
          if(typeOfBoard === 'content'){
            that.item.content[pageId].drawings =[];
          }
          that.paint.resetZoom(true,true);
        })
      }
    },
    watch: {
      socket: function (newVal,oldVal) {
        this.listenToSocket();
      }
    },
    mounted: function () {
      let that=this;
      let currentPage;
      that.paint=require('../paint/paint')(that.item.id)

      if(that.item.type==='content'){
        let currentPage=that.getCurrentPage();
        if(that.item.new){
          that.switchPage();
        }
      }
      that.paint.addEventListener("drawing", function (event) {
        delete event.target;
        if(that.item.type==='content'){
          currentPage=that.getCurrentPage();
          currentPage.drawings.push(event.drawing);
          that.socket.emit('drawing',event.drawing,that.item.id,'content',currentPage.id);  
        }else{
          that.socket.emit('drawing',event.drawing,that.item.id,'whiteboard');
        }
      });
      that.paint.addEventListener("drawingchange", function (event) {
        delete event.target;
        if(that.item.type==='content'){
          currentPage=that.getCurrentPage();
          that.socket.emit('drawingchange',event.drawing,that.item.id,'content',currentPage.id);  
        }else{
          that.socket.emit('drawingchange',event.drawing,that.item.id, 'whiteboard');
        }
      });
      that.paint.addEventListener("zoom", function (event) {
        delete event.target;
        if(that.item.type==='content'){
          currentPage=that.getCurrentPage();
          that.socket.emit('zoom',event,that.item.id,'content',currentPage.id);  
        }else{
          that.socket.emit('zoom',event,that.item.id, 'whiteboard');
        }
      });
      that.paint.addEventListener("move", function (event) {
        delete event.target;
        if(that.item.type==='content'){
          currentPage=that.getCurrentPage();
          that.socket.emit('move',event,that.item.id,'content',currentPage.id);  
        }else{
          that.socket.emit('move',event,that.item.id, 'whiteboard');
        }
      });
      that.paint.addEventListener("grid", function (event) {
        delete event.target;
        if(that.item.type==='content'){
          currentPage=that.getCurrentPage();
          that.socket.emit('grid',event,that.item.id,'content',currentPage.id);  
        }else{
          that.socket.emit('grid',event,that.item.id, 'whiteboard');
        }
      });
      that.paint.addEventListener("clear", function (event) {
        if(that.item.type==='content'){
          currentPage=that.getCurrentPage();
          currentPage.drawings = [];
          that.socket.emit('clear',that.item.id,'content',currentPage.id);  
        }else{
          that.socket.emit('clear',that.item.id, 'whiteboard');
        }
      });
      that.paint.addEventListener("undo", function (event) {
        if(that.item.type==='content'){
          currentPage=that.getCurrentPage();
          currentPage.drawings.pop();
          that.socket.emit('undo',that.item.id,'content',currentPage.id);  
        }else{
          that.socket.emit('undo',that.item.id, 'whiteboard');
        }
      });
      that.paint.addEventListener("removedrawing", function (event) {
        if(that.item.type==='content'){
          currentPage=that.getCurrentPage();
          that.socket.emit('removedrawing', event.id, that.item.id,'content',currentPage.id);  
        }else{
          that.socket.emit('removedrawing', event.id, that.item.id, 'whiteboard');
        }
      });
      that.paint.addEventListener("resetworkspace", function () {
        if(that.item.type==='content'){
          currentPage=that.getCurrentPage();
          that.socket.emit('resetworkspace', that.item.id,'content',currentPage.id);  
        }else{
          that.socket.emit('resetworkspace', that.item.id, 'whiteboard');
        }
      });
      
      // window.addEventListener('resize', function (event){
      //   //that.switchPage(true);
      // });

      if(that.socket){
        that.listenToSocket();
      }
    }
  }